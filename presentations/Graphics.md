footer: :copyright: Adrian Kosmaczewski - 2017 - All Rights Reserved
slidenumbers: true
theme: Zurich, 6

# Graphics

## Android for iOS Developers
### Adrian Kosmaczewski
![inline left 30%](../images/logo.png)

---

| Density Bucket | Screen Density | Physical Size | Pixel Size                    |
|----------------|----------------|---------------|-------------------------------|
| ldpi           | 120 dpi        | 0.5 x 0.5 in  | 0.5 in * 120 dpi = 60x60 px   |
| mdpi           | 160 dpi        | 0.5 x 0.5 in  | 0.5 in * 160 dpi = 80x80 px   |
| hdpi           | 240 dpi        | 0.5 x 0.5 in  | 0.5 in * 240 dpi = 120x120 px |
| xhdpi          | 320 dpi        | 0.5 x 0.5 in  | 0.5 in * 320 dpi = 160x160 px |
| xxhdpi         | 480 dpi        | 0.5 x 0.5 in  | 0.5 in * 480 dpi = 240x240 px |
| xxxhdpi        | 640 dpi        | 0.5 x 0.5 in  | 0.5 in * 640 dpi = 320x320 px |

---

|                                   | Android                      | iOS                                    |
|-----------------------------------|------------------------------|----------------------------------------|
| Framework                         | `android.graphics`           | UIKit                                  |
| Views                             | `View`                       | `UIView`                               |
| Coordinate system                 | Origin at top left           | Origin at top left                     |
| Location on screen                | `LayoutParams`               | `CGRect`                               |
| Images                            | `ImageView`                  | `UIImageView`                          |
| Colors                            | `Color` (manipulates `int`!) | `UIColor`                              |
| Bezier curves                     | `Path`                       | `UIBezierPath`                         |
| Drawing method                    | `onDraw()`                   | `draw()`                               |
| Drawing context                   | `Canvas`                     | `CGContext`                            |
| Mark as "dirty"                   | `invalidate()`               | `setNeedsDisplay()`                    |
| Gestures                          | `GestureDetector`            | `UIGestureRecognizer`                  |
| Pinch gesture                     | `ScaleGestureDetector`       | `UIPinchGestureRecognizer`             |
| Affine Transformations            | `Matrix`                     | `CGAffineTransform`                    |
| Simple animations                 | `View.animate()`             | `animate(withDuration:animations:)`    |
| Complex animations                | `android.animation.Animator` | `CAAnimation`                          |
| Application-level memory warnings | `Application.onLowMemory()`  | `applicationDidReceiveMemoryWarning()` |
| Activity-level memory warnings    | `Activity.onTrimMemory()`    | `didReceiveMemoryWarning()`            |

---

| Unit | Description                | Units Per Physical Inch | Density Independent | Same Physical Size On Every Screen |
|------|----------------------------|-------------------------|---------------------|------------------------------------|
| px   | Pixels                     | Varies                  | No                  | No                                 |
| in   | Inches                     | 1                       | Yes                 | Yes                                |
| mm   | Millimeters                | 25.4                    | Yes                 | Yes                                |
| pt   | Points                     | 72                      | Yes                 | Yes                                |
| dp   | Density Independent Pixels | ~160                    | Yes                 | No                                 |
| sp   | Scale Independent Pixels   | ~160                    | Yes                 | No                                 |

---

![inline](../images/Graphics/Drawing.png)

---

![inline](../images/Graphics/Paintcode_app.png)

---

![inline](../images/Graphics/Paintcode_device.png)

---

![inline](../images/Graphics/Size_smartphone.png)

---

![inline](../images/Graphics/Size_tablet.png)

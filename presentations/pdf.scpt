-- This AppleScript drives Deckset to generate all the PDF files
-- that correspond to the current Markdown source files

tell application "Finder"
    set current_path to container of (path to me) as alias
	set presentations to name of files of current_path whose name extension is "md"
end tell

tell application "Deckset"
    repeat with presentation in presentations
        set aFile to ((the POSIX path of current_path) & presentation as POSIX file)
        set currentFile to open aFile
        activate currentFile
        tell application "System Events"
            tell process "Deckset"
                click menu item "Export..." of menu 1 of menu bar item "File" of menu bar 1
                delay 2
                click button "Next..." of sheet of window 1
                delay 2
                click button "Export" of sheet of window 1
            end tell
        end tell
        delay 2
        close currentFile
    end repeat
end tell


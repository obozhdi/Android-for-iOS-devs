footer: :copyright: Adrian Kosmaczewski - 2017 - All Rights Reserved
slidenumbers: true
theme: Zurich, 6

# Networking

## Android for iOS Developers
### Adrian Kosmaczewski
![inline left 30%](../images/logo.png)

---

|                            | Android                        | iOS                            |
|----------------------------|--------------------------------|--------------------------------|
| Native networking library  | `HttpURLConnection`            | `NSURLConnection`              |
| Background mechanism       | `android.os.AsyncTask`         | `NSOperation`                  |
| JSON Parser                | `org.json.JSONObject`          | `NSJSONSerialization`          |
| JSON (de)serialization     | Gson                           | `NSPropertlyListSerialization` |
| XML SAX                    | `org.xmlpull.v1.XmlPullParser` | `NSXMLParser`                  |
| XML DOM                    | `org.w3c.dom.Document`         | KissXML                        |
| Array                      | `List<>` & `ArrayList<>`       | `NSArray` & `NSMutableArray`   |
| Table view                 | `RecyclerView`                 | `UITableView`                  |
| Table view data            | `RecyclerView.Adapter`         | `UITableViewDataSource`        |
| Table view cell            | `RecyclerView.ViewHolder`      | `UITableViewCell`              |
| REST Client                | Retrofit                       | RESTKit                        |
| Popular networking library | OkHttp                         | AFNetworking                   |

---

![inline](../images/Networking/Library_dependency_01.png)

---

![inline](../images/Networking/Library_dependency_02.png)

---

![inline](../images/Networking/ListView.png)

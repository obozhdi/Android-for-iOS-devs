footer: :copyright: Adrian Kosmaczewski - 2017 - All Rights Reserved
slidenumbers: true
theme: Zurich, 6

# Storage

## Android for iOS Developers
### Adrian Kosmaczewski
![inline left 30%](../images/logo.png)

---

|                   | Android                                     | iOS                                     |
|-------------------|---------------------------------------------|-----------------------------------------|
| Local documents   | `Context.getFilesDir()`                     | `NSSearchPathForDirectoriesInDomains()` |
| External storage  | `Environment.getExternalStorageDirectory()` | n/a                                     |
| Bundled resource  | `getResources()`                            | `NSBundle`                              |
| Downloading files | `DownloadManager`                           | `UIBackgroundTaskIdentifier`            |
| Notifications     | `BroadcastReceiver`                         | `NSNotificationCenter`                  |
| Periodic tasks    | `android.os.Handler` & `Runnable`           | `NSTask`                                |
| Preferences       | `PreferenceManager` & `SharedPreferences`   | `NSUserDefaults`                        |
| Sqlite wrapper    | SQLiteOpenHelper                            | FMDB                                    |
| ORM               | OrmLite                                     | Core Data                               |
| Realm             | Realm                                       | Realm                                   |

---

![inline](../images/Storage/Android_monitor_transfer_files.png)


footer: :copyright: Adrian Kosmaczewski - 2017 - All Rights Reserved
slidenumbers: true
theme: Zurich, 6

# Testing

## Android for iOS Developers
### Adrian Kosmaczewski
![inline left 30%](../images/logo.png)

---

|                        | Android                | iOS                                        |
|------------------------|------------------------|--------------------------------------------|
| Unit testing framework | JUnit 4                | XCTest                                     |
| Mock objects framework | Mockito                | OCMock – OCMockito                         |
| Most common issue      | `NullPointerException` | (ObjC) Messages to nil & (Swift) optionals |

---

![inline](../images/Testing/Code_coverage.png)

---

![inline](../images/Testing/Coverage_report.png)

---

![inline](../images/Testing/Findbugs_install.png)

---

![inline](../images/Testing/Test_configuration.png)

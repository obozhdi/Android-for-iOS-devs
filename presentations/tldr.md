footer: :copyright: Adrian Kosmaczewski - 2017 - All Rights Reserved
slidenumbers: true
theme: Zurich, 6

# TL;DR

## Android for iOS Developers
### Adrian Kosmaczewski
![inline left 30%](../images/logo.png)

---

# Architecture

|                      | Android                        | iOS                            |
|----------------------|--------------------------------|--------------------------------|
| Notification center  | `LocalBroadcastManager`        | `NSNotificationCenter`         |
| Flexible designs     | Interface-oriented programming | Protocol-oriented programming  |
| Dependency injection | Dagger                         | Typhoon – Swinject – Cleanse … |
| ReactiveX            | RxJava & RxAndroid             | RxSwift                        |
| Observing data       | `rx.Observable`                | `willSet` & `didSet` – KVO     |

---

# Debugging

|                  | Android          | iOS                              |
|------------------|------------------|----------------------------------|
| Debugger         | JDB              | LLDB                             |
| Log output       | logcat           | Xcode console                    |
| Remote debugging | yes              | no                               |
| Log viewers      | PID Cat & LogCat | libimobiledevice & deviceconsole |
| Network logger   | NSLogger         | NSLogger                         |

---

# Graphics

|                                   | Android                      | iOS                                    |
|-----------------------------------|------------------------------|----------------------------------------|
| Framework                         | `android.graphics`           | UIKit                                  |
| Views                             | `View`                       | `UIView`                               |
| Coordinate system                 | Origin at top left           | Origin at top left                     |
| Location on screen                | `LayoutParams`               | `CGRect`                               |
| Images                            | `ImageView`                  | `UIImageView`                          |
| Colors                            | `Color` (manipulates `int`!) | `UIColor`                              |
| Bezier curves                     | `Path`                       | `UIBezierPath`                         |
| Drawing method                    | `onDraw()`                   | `draw()`                               |
| Drawing context                   | `Canvas`                     | `CGContext`                            |
| Mark as "dirty"                   | `invalidate()`               | `setNeedsDisplay()`                    |
| Gestures                          | `GestureDetector`            | `UIGestureRecognizer`                  |
| Pinch gesture                     | `ScaleGestureDetector`       | `UIPinchGestureRecognizer`             |
| Affine Transformations            | `Matrix`                     | `CGAffineTransform`                    |
| Simple animations                 | `View.animate()`             | `animate(withDuration:animations:)`    |
| Complex animations                | `android.animation.Animator` | `CAAnimation`                          |
| Application-level memory warnings | `Application.onLowMemory()`  | `applicationDidReceiveMemoryWarning()` |
| Activity-level memory warnings    | `Activity.onTrimMemory()`    | `didReceiveMemoryWarning()`            |

---

# Multimedia

|                       | Android                           | iOS                           |
|-----------------------|-----------------------------------|-------------------------------|
| Display image         | `android.widget.ImageView`        | `UIImageView`                 |
| Display video         | `android.widget.VideoView`        | `MPMoviePlayerViewController` |
| Viewer application    | Gallery                           | Photos                        |
| Image picker          | `Intent.ACTION_PICK`              | `UIImagePickerController`     |
| Audio recorder        | `android.media.MediaRecorder`     | `AVAudioRecorder`             |
| Audio player          | `android.media.MediaPlayer`       | `AVAudioPlayer`               |
| Text to speech engine | `android.speech.tts.TextToSpeech` | `AVSpeechSynthesizer`         |

---

# Networking

|                            | Android                        | iOS                            |
|----------------------------|--------------------------------|--------------------------------|
| Native networking library  | `HttpURLConnection`            | `NSURLConnection`              |
| Background mechanism       | `android.os.AsyncTask`         | `NSOperation`                  |
| JSON Parser                | `org.json.JSONObject`          | `NSJSONSerialization`          |
| JSON (de)serialization     | Gson                           | `NSPropertlyListSerialization` |
| XML SAX                    | `org.xmlpull.v1.XmlPullParser` | `NSXMLParser`                  |
| XML DOM                    | `org.w3c.dom.Document`         | KissXML                        |
| Array                      | `List<>` & `ArrayList<>`       | `NSArray` & `NSMutableArray`   |
| Table view                 | `RecyclerView`                 | `UITableView`                  |
| Table view data            | `RecyclerView.Adapter`         | `UITableViewDataSource`        |
| Table view cell            | `RecyclerView.ViewHolder`      | `UITableViewCell`              |
| REST Client                | Retrofit                       | RESTKit                        |
| Popular networking library | OkHttp                         | AFNetworking                   |

---

# Sensors

|                  | Android               | iOS                                                 |
|------------------|-----------------------|-----------------------------------------------------|
| Framework        | `android.hardware`    | Core Motion & Core Location                         |
| Main class       | `SensorManager`       | `CMMotionManager`                                   |
| Callback methods | `SensorEventListener` | Blocks                                              |
| Sensor data      | `SensorEvent`         | `CMGyroData` – `CMAccelerometerData` – `CMAttitude` |
| Location         | `LocationManager`     | `CLLocationManager`                                 |

---

# Storage

|                   | Android                                     | iOS                                     |
|-------------------|---------------------------------------------|-----------------------------------------|
| Local documents   | `Context.getFilesDir()`                     | `NSSearchPathForDirectoriesInDomains()` |
| External storage  | `Environment.getExternalStorageDirectory()` | n/a                                     |
| Bundled resource  | `getResources()`                            | `NSBundle`                              |
| Downloading files | `DownloadManager`                           | `UIBackgroundTaskIdentifier`            |
| Notifications     | `BroadcastReceiver`                         | `NSNotificationCenter`                  |
| Periodic tasks    | `android.os.Handler` & `Runnable`           | `NSTask`                                |
| Preferences       | `PreferenceManager` & `SharedPreferences`   | `NSUserDefaults`                        |
| Sqlite wrapper    | SQLiteOpenHelper                            | FMDB                                    |
| ORM               | OrmLite                                     | Core Data                               |
| Realm             | Realm                                       | Realm                                   |

---

# Testing

|                        | Android                | iOS                                        |
|------------------------|------------------------|--------------------------------------------|
| Unit testing framework | JUnit 4                | XCTest                                     |
| Mock objects framework | Mockito                | OCMock – OCMockito                         |
| Most common issue      | `NullPointerException` | (ObjC) Messages to nil & (Swift) optionals |

---

# Toolchain

|                              | Android                           | iOS                   |
|------------------------------|-----------------------------------|-----------------------|
| IDE                          | Android Studio                    | Xcode                 |
| Profiling                    | Android Device Monitor            | Instruments           |
| Preview                      | Android Emulator                  | iOS Simulator         |
| Blocks in previous versions  | Retrolambda                       | PLBlocks              |
| Programming Language         | Java                              | Swift or Objective-C  |
| Command Line                 | `gradlew` – `ant`                 | `xcodebuild`          |
| Going beyond                 | Rooting                           | Jailbreaking          |
| Application metadata         | AndroidManifest.xml               | Info.plist            |
| Dependency Manager           | Gradle                            | CocoaPods – Carthage  |
| Distribution                 | APK                               | IPA                   |
| Debugger                     | ADB + DDMS                        | LLDB                  |
| Logger                       | LogCat                            | NSLog() or print()    |
| View Debugging               | Hierarchy viewer                  | Xcode view debugging  |
| Static Analysis              | Android Lint                      | Clang Static Analyzer |
| Classic programming language | Java                              | Objective-C           |
| Hype programming language    | Kotlin – Groovy – Scala – Clojure | Swift                 |

---

# UI

|                                | Android                    | iOS                           |
|--------------------------------|----------------------------|-------------------------------|
| UI design                      | Layout files               | NIB/XIB/Storyboard            |
| Controllers                    | Activity                   | UIViewController              |
| Callbacks                      | Anonymous Classes          | `IBAction`                    |
| Views                          | `android.view.View`        | `UIView`                      |
| Connecting views               | `findViewById(R.id.xxxxx)` | `IBOutlet`                    |
| Text fields                    | `EditText`                 | `UITextField`                 |
| Buttons                        | `Button`                   | `UIButton`                    |
| Text labels                    | `TextView`                 | `UILabel`                     |
| Translatable strings           | strings.xml                | Localizable.strings           |
| Navigation between controllers | Intent                     | Storyboard Segue              |
| UI decomposition               | Fragment                   | Children UIViewController     |
| Serialization                  | `Parcelable`               | `NSPropertyListSerialization` |
| Dialog boxes                   | `AlertDialog`              | `UIAlertController`           |

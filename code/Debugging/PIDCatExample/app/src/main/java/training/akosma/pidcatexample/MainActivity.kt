package training.akosma.pidcatexample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener {
            Log.i(this.javaClass.name, "Button clicked")
        }

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                Log.i(this.javaClass.name, "Seekbar changed")
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                Log.i(this.javaClass.name, "Seekbar onStartTrackingTouch")
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                Log.i(this.javaClass.name, "Seekbar onStopTrackingTouch")
            }
        })
    }
}

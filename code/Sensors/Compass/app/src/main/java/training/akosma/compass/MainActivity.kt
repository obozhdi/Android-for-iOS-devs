package training.akosma.compass

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

// Adapted from
// http://www.techrepublic.com/article/pro-tip-create-your-own-magnetic-compass-using-androids-internal-sensors/
class MainActivity : AppCompatActivity(), SensorEventListener {

    // Arrow picture coming by Shirly Niv Marton
    // https://unsplash.com/search/arrow?photo=6E6CMgFAUjc
    private var sensorManager: SensorManager? = null
    private var magnetometer: Sensor? = null
    private var accelerometer: Sensor? = null

    private var lastAccData = FloatArray(3)
    private var lastMagnData = FloatArray(3)
    private var isAccDataReady = false
    private var isMagnDataReady = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        magnetometer = sensorManager?.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)
        accelerometer = sensorManager?.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
    }

    override fun onResume() {
        super.onResume()
        sensorManager?.registerListener(this,
                magnetometer,
                SensorManager.SENSOR_DELAY_GAME)
        sensorManager?.registerListener(this,
                accelerometer,
                SensorManager.SENSOR_DELAY_GAME)
    }

    override fun onPause() {
        super.onPause()
        sensorManager?.unregisterListener(this, magnetometer)
        sensorManager?.unregisterListener(this, accelerometer)
    }

    // tag::onchanged[]
    override fun onSensorChanged(sensorEvent: SensorEvent) {
        if (sensorEvent.sensor == accelerometer) {
            lastAccData = sensorEvent.values.clone()
            isAccDataReady = true
        } else if (sensorEvent.sensor == magnetometer) {
            lastMagnData = sensorEvent.values.clone()
            isMagnDataReady = true
        }

        if (isAccDataReady && isMagnDataReady) {
            val rotationMatrix = FloatArray(9)
            SensorManager.getRotationMatrix(rotationMatrix, null,
                    lastAccData,
                    lastMagnData)

            val orientation = FloatArray(3)
            SensorManager.getOrientation(rotationMatrix, orientation)

            val radians = orientation[0]
            val degrees = Math.toDegrees(radians.toDouble()).toFloat()
            val rotation = (degrees + 360) % 360
            view_compass.rotation = -rotation
        }
    }
    // end::onchanged[]

    override fun onAccuracyChanged(sensor: Sensor, i: Int) {}
}

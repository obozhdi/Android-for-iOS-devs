#include <jni.h>
#include <string>
#include "rsalib.h"

using std::string;

static jclass manager_class;
static jfieldID p_field;
static jfieldID q_field;
static jfieldID e_field;
static jfieldID d_field;
static jfieldID n_field;

string valueOfField(JNIEnv *env,
                    jobject thisObj,
                    jfieldID field) {
    jstring pobj = (jstring) env->GetObjectField(thisObj, field);
    const char *chars = env->GetStringUTFChars(pobj, NULL);
    string result{chars};
    env->ReleaseStringUTFChars(pobj, chars);
    return result;
}

void setValueOfField(JNIEnv *env,
                     jobject thisObj,
                     jfieldID field,
                     const char *value) {
    jstring newValue = env->NewStringUTF(value);
    env->SetObjectField(thisObj, field, newValue);
}

extern "C"
JNIEXPORT void JNICALL
Java_training_akosma_rsa_RSAManager_initialize(
        JNIEnv *env,
        jobject thisObj) {

    manager_class = env->GetObjectClass(thisObj);
    p_field = env->GetFieldID(manager_class, "p", "Ljava/lang/String;");
    q_field = env->GetFieldID(manager_class, "q", "Ljava/lang/String;");
    e_field = env->GetFieldID(manager_class, "e", "Ljava/lang/String;");
    d_field = env->GetFieldID(manager_class, "d", "Ljava/lang/String;");
    n_field = env->GetFieldID(manager_class, "n", "Ljava/lang/String;");
}

extern "C"
JNIEXPORT void JNICALL
Java_training_akosma_rsa_RSAManager_destroy(
        JNIEnv *env,
        jobject thisObj) {
}

extern "C"
JNIEXPORT void JNICALL
Java_training_akosma_rsa_RSAManager_keys(
        JNIEnv *env,
        jobject thisObj,
        jstring msg) {

    string p = valueOfField(env, thisObj, p_field);
    string q = valueOfField(env, thisObj, q_field);
    string e = valueOfField(env, thisObj, e_field);

    keyset keys = rsa_keys(p, q, e);
    string ds = keys["private"].first;
    string ns = keys["private"].second;
    setValueOfField(env, thisObj, d_field, ds.c_str());
    setValueOfField(env, thisObj, n_field, ns.c_str());
}

// tag::encrypt[]
extern "C"
JNIEXPORT jstring JNICALL
Java_training_akosma_rsa_RSAManager_encrypt(
        JNIEnv *env,
        jobject thisObj,
        jstring msg) {

    string n = valueOfField(env, thisObj, n_field);
    string e = valueOfField(env, thisObj, e_field);
    const char *message = env->GetStringUTFChars(msg, JNI_FALSE);
    string result = rsa_encrypt(message, e, n);
    env->ReleaseStringUTFChars(msg, message);
    return env->NewStringUTF(result.c_str());
}
// end::encrypt[]

extern "C"
JNIEXPORT jstring JNICALL
Java_training_akosma_rsa_RSAManager_decrypt(
        JNIEnv *env,
        jobject thisObj,
        jstring msg) {

    string n = valueOfField(env, thisObj, n_field);
    string d = valueOfField(env, thisObj, d_field);
    const char *message = env->GetStringUTFChars(msg, JNI_FALSE);
    string result = rsa_decrypt(message, d, n);
    env->ReleaseStringUTFChars(msg, message);
    return env->NewStringUTF(result.c_str());
}

import { Calc } from "./Calc";

declare global {
    namespace NodeJS {
        interface Global {
            calc: Calc
        }
    }
}

global.calc = new Calc();

const button = document.getElementById("btn")! as HTMLButtonElement;
const label = document.getElementById("label")! as HTMLSpanElement;

button.onclick = async function () {
    label.innerHTML = "executing…";
    let a = await global.calc.longCalculation(1000000000)
    label.innerHTML = a.toString();
}

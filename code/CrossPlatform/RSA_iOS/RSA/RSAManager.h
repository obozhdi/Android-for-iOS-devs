#import <Foundation/Foundation.h>

@interface RSAManager : NSObject

@property (nonatomic, copy) NSString * _Nonnull p;
@property (nonatomic, copy) NSString * _Nonnull q;
@property (nonatomic, copy) NSString * _Nonnull e;
@property (nonatomic, copy) NSString * _Nonnull n;
@property (nonatomic, copy) NSString * _Nonnull d;

- (void)keys;

- (nonnull NSString *)encrypt:(nonnull NSString *)message;

- (nonnull NSString *)decrypt:(nonnull NSString *)message;

@end

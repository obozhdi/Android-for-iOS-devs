package training.akosma.ormlite

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter

import com.j256.ormlite.android.apptools.OpenHelperManager
import com.j256.ormlite.dao.RuntimeExceptionDao
import kotlinx.android.synthetic.main.activity_main.*
import java.util.Random

class MainActivity : AppCompatActivity() {

    private var databaseHelper: DatabaseHelper? = null
    private var dao: RuntimeExceptionDao<Person, Int>? = null
    internal var random = Random()

    private val helper: DatabaseHelper
        get() {
            if (databaseHelper == null) {
                databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper::class.java)
            }
            return databaseHelper!!
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // tag::usage[]
        dao = helper.getPersonDao()

        val john = Person()
        john.name = "John Smith"
        john.age = 35
        dao?.create(john)

        val lucy = Person()
        lucy.name = "Lucy Skies"
        lucy.age = 42
        dao?.create(lucy)

        showAllPeople()
        // end::usage[]
    }

    override fun onDestroy() {
        super.onDestroy()
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper()
            databaseHelper = null
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.app_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.add -> {
                val name = generateRandomName() + " " + generateRandomName()
                val age = generateRandomNumberBetween(15, 75)

                val newPerson = Person()
                newPerson.name = name
                newPerson.age = age
                dao?.create(newPerson)

                showAllPeople()

                return true
            }

            R.id.clean -> {
                val people = dao?.queryForAll()
                dao?.delete(people)
                showAllPeople()
                return super.onOptionsItemSelected(item)
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun showAllPeople() {
        val people = dao?.queryForAll()
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, people)
        listView.adapter = adapter
    }

    // Adapted from
    // https://stackoverflow.com/a/5271613/133764
    private fun generateRandomNumberBetween(min: Int, max: Int): Int {
        return random.nextInt(max - min) + min
    }

    // Adapted from
    // https://stackoverflow.com/a/12116327/133764
    private fun generateRandomName(): String {
        val randomStringBuilder = StringBuilder()
        var tempChar = generateRandomNumberBetween(65, 90).toChar()
        randomStringBuilder.append(tempChar)
        val randomLength = generateRandomNumberBetween(5, 15)
        for (i in 0 until randomLength) {
            tempChar = generateRandomNumberBetween(97, 122).toChar()
            randomStringBuilder.append(tempChar)
        }
        return randomStringBuilder.toString()
    }
}

package training.akosma.browser

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // tag::setup[]
        val webSettings = webView.settings
        webSettings.builtInZoomControls = true
        webView.webViewClient = Client()

        goButton.setOnClickListener { go() }

        urlField.setImeActionLabel(getString(R.string.go), KeyEvent.KEYCODE_ENTER)
        urlField.setOnEditorActionListener { textView, i, keyEvent ->
            if (keyEvent != null && keyEvent.keyCode == 66) {
                go()
            }
            false
        }
        // end::setup[]
    }

    // tag::go[]
    private fun go() {
        webView.requestFocus()
        val input = urlField.text.toString()
        webView.loadUrl(input)
    }
    // end::go[]

    // tag::back[]
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    if (webView.canGoBack()) {
                        webView.goBack()
                    } else {
                        finish()
                    }
                    return true
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }
    // end::back[]

    // tag::client[]
    private inner class Client : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
            return false
        }

        override fun onPageFinished(view: WebView, url: String) {
            urlField.setText(webView.url)
            webView.requestFocus()
        }
    }
    // end::client[]
}

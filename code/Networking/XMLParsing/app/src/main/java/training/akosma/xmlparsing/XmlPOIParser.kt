package training.akosma.xmlparsing

import android.util.Xml
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.io.InputStream
import java.util.*

internal class XmlPOIParser {

    @Throws(XmlPullParserException::class, IOException::class)
    fun parse(inputStream: InputStream): List<PointOfInterest> {
        try {
            val parser = Xml.newPullParser()
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
            parser.setInput(inputStream, null)
            parser.nextTag()
            return readFeed(parser)
        } finally {
            inputStream.close()
        }
    }

    // tag::parse[]
    @Throws(XmlPullParserException::class, IOException::class)
    private fun readFeed(parser: XmlPullParser): List<PointOfInterest> {
        val points = ArrayList<PointOfInterest>()

        parser.require(XmlPullParser.START_TAG, ns, "geonames")
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            val name = parser.name
            // Starts by looking for the entry tag
            if (name == "entry") {
                points.add(readPOI(parser))
            } else {
                skip(parser)
            }
        }
        return points
    }
    // end::parse[]

    // tag::read[]
    @Throws(XmlPullParserException::class, IOException::class)
    private fun readPOI(parser: XmlPullParser): PointOfInterest {
        parser.require(XmlPullParser.START_TAG, ns, "entry")
        val poi = PointOfInterest()
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            val name = parser.name
            when (name) {
                "lang" -> poi.lang = readString(parser, "lang")
                "title" -> poi.title = readString(parser, "title")
                "summary" -> poi.summary = readString(parser, "summary")
                "feature" -> poi.feature = readString(parser, "feature")
                "countryCode" -> poi.countryCode = readString(parser, "countryCode")
                "wikipediaUrl" -> poi.wikipediaUrl = readString(parser, "wikipediaUrl")
                "distance" -> poi.distance = readString(parser, "distance")
                "elevation" -> poi.elevation = readInt(parser, "elevation")
                "geoNameId" -> poi.geoNameId = readInt(parser, "geoNameId")
                "lat" -> poi.lat = readDouble(parser, "lat")
                "lng" -> poi.lng = readDouble(parser, "lng")
                "rank" -> poi.rank = readInt(parser, "rank")
                else -> skip(parser)
            }
        }
        return poi
    }
    // end::read[]

    @Throws(IOException::class, XmlPullParserException::class)
    private fun readString(parser: XmlPullParser, tag: String): String {
        parser.require(XmlPullParser.START_TAG, ns, tag)
        val title = readText(parser)
        parser.require(XmlPullParser.END_TAG, ns, tag)
        return title
    }

    @Throws(IOException::class, XmlPullParserException::class)
    private fun readInt(parser: XmlPullParser, tag: String): Int {
        val text = readString(parser, tag)
        return Integer.valueOf(text)!!
    }

    @Throws(IOException::class, XmlPullParserException::class)
    private fun readDouble(parser: XmlPullParser, tag: String): Double {
        val text = readString(parser, tag)
        return java.lang.Double.valueOf(text)!!
    }

    @Throws(IOException::class, XmlPullParserException::class)
    private fun readText(parser: XmlPullParser): String {
        var result = ""
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.text
            parser.nextTag()
        }
        return result
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun skip(parser: XmlPullParser) {
        if (parser.eventType != XmlPullParser.START_TAG) {
            throw IllegalStateException()
        }
        var depth = 1
        while (depth != 0) {
            when (parser.next()) {
                XmlPullParser.END_TAG -> depth--
                XmlPullParser.START_TAG -> depth++
            }
        }
    }

    companion object {
        // We don't use namespaces
        private val ns: String? = null
    }
}

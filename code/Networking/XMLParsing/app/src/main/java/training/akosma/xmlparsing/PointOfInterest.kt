package training.akosma.xmlparsing

/*
This class maps the following structure to a POJO:

{
"summary": "The Glärnisch is a mountain of the North-Eastern Swiss Alps, overlooking the valley of the Linth in the Swiss canton of Glarus. It consists of several summits of which the highest is 2,915 metres above sea level (...)",
"elevation": 2880,
"geoNameId": 2660595,
"feature": "mountain",
"lng": 8.998611,
"distance": "0.1869",
"countryCode": "CH",
"rank": 91,
"lang": "en",
"title": "Glärnisch",
"lat": 46.998611,
"wikipediaUrl": "en.wikipedia.org/wiki/Gl%C3%A4rnisch"
},
 */

import android.graphics.Point

import org.json.JSONException
import org.json.JSONObject

import java.util.Locale

class PointOfInterest {
    var summary: String? = null
    var elevation: Int = 0
    var geoNameId: Int = 0
    var feature: String? = null
    var lng: Double = 0.toDouble()
    var distance: String? = null
    var countryCode: String? = null
    var rank: Int = 0
    var lang: String? = null
    var title: String? = null
    var lat: Double = 0.toDouble()
    var wikipediaUrl: String? = null

    internal constructor() {}

    @Throws(JSONException::class)
    internal constructor(obj: JSONObject) {
        summary = obj.getString("summary")
        elevation = obj.getInt("elevation")
        if (obj.has("geoNameId")) {
            geoNameId = obj.getInt("geoNameId")
        }
        if (obj.has("feature")) {
            feature = obj.getString("feature")
        }
        lng = obj.getDouble("lng")
        distance = obj.getString("distance")
        countryCode = obj.getString("countryCode")
        rank = obj.getInt("rank")
        lang = obj.getString("lang")
        title = obj.getString("title")
        lat = obj.getDouble("lat")
        wikipediaUrl = obj.getString("wikipediaUrl")
    }

    override fun toString(): String {
        return String.format(Locale.ENGLISH, "%s (%s : %s)", title, lat, lng)
    }
}

package training.akosma.xmlparsing

import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONException
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ConnectToAPITask().execute()
    }

    private fun updateList(points: List<PointOfInterest>) {
        Log.i("MainActivity", "Success, passing points to the main thread: " + points)
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, points)
        listView.adapter = adapter
    }

    private inner class ConnectToAPITask : AsyncTask<Void, Void, List<PointOfInterest>>() {
        override fun doInBackground(vararg voids: Void): List<PointOfInterest>? {
            try {
                val connector = APIConnector()
                connector.format = APIConnectorFormat.XML
                return connector.pointsOfInterest
            } catch (e: IOException) {
                Log.e("MainActivity", "Failed to retrieve data", e)
                cancel(true)
            } catch (e: JSONException) {
                Log.e("MainActivity", "Failed to parse JSON", e)
                cancel(true)
            } catch (e: XmlPullParserException) {
                Log.e("MainActivity", "Failed to parse XML", e)
            }

            return null
        }

        override fun onPostExecute(points: List<PointOfInterest>) {
            Log.i("MainActivity", "Success, passing points to the main thread: " + points)
            updateList(points)
        }

        override fun onCancelled() {
            Log.i("MainActivity", "BOOM")
        }
    }
}

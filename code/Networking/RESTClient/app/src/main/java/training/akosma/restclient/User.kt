package training.akosma.restclient

// tag::user[]
class User {
    // end::user[]
    var name: String? = null
    var age: Int = 0
    var country: String? = null

    override fun toString(): String {
        val sb = StringBuilder(name)
        sb.append("; age: ")
                .append(age)
                .append(", country: ")
                .append(country)
        return sb.toString()
    }
}

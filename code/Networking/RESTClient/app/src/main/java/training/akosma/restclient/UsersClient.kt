package training.akosma.restclient

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

// tag::client[]
interface UsersClient {
    @get:GET("/users")
    val users: Call<List<User>>

    @GET("/user/{id}")
    fun getUser(@Path("id") userId: Int): Call<User>

    @POST("/users")
    fun createUser(@Body user: User): Call<User>

    @PUT("/user/{id}")
    fun updateUser(@Path("id") userId: Int, @Body user: User): Call<Void>

    @DELETE("/user/{id}")
    fun deleteUser(@Path("id") userId: Int): Call<Void>
}
// end::client[]

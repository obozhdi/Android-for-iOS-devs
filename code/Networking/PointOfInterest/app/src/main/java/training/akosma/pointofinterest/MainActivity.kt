package training.akosma.pointofinterest

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager

// tag::fragment[]
class MainActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fm = supportFragmentManager
        var fragment: Fragment? = fm.findFragmentById(R.id.fragment)

        if (fragment == null) {
            fragment = PointOfInterestListFragment()
            fm.beginTransaction()
                    .add(R.id.fragment, fragment)
                    .commit()
        }
    }
}
// end::fragment[]

package training.akosma.jsonparsing

enum class APIConnectorFormat private constructor(val value: String) {
    JSON("JSON"),
    XML("")
}

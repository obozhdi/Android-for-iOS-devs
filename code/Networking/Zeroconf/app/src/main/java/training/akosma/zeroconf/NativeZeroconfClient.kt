package training.akosma.zeroconf

import android.content.Context
import android.net.nsd.NsdManager
import android.net.nsd.NsdServiceInfo
import android.util.Log

import java.util.Locale

internal class NativeZeroconfClient(context: Context) : ZeroconfClient {
    private val nsdManager: NsdManager?
    private var resolveListener: NsdManager.ResolveListener? = null
    private var discoveryListener: NsdManager.DiscoveryListener? = null
    private var backingService: NsdServiceInfo? = null
    private var listener: ZeroconfClientListener? = null

    override val service: String?
        get() {
            if (backingService == null) {
                return null
            }
            val port = backingService?.port
            val host = backingService?.host
            return String.format(Locale.ENGLISH, "%s:%d", host, port)
        }

    init {
        nsdManager = context.getSystemService(Context.NSD_SERVICE) as NsdManager
    }

    override fun setListener(listener: ZeroconfClientListener) {
        this.listener = listener
    }

    override fun start() {
        initializeResolveListener()
        initializeDiscoveryListener()
        startDiscovery()
    }

    override fun stop() {
        stopDiscovery()
        resolveListener = null
        discoveryListener = null
        backingService = null
    }

    private fun startDiscovery() {
        // tag::start[]
        nsdManager?.discoverServices(Constants.SERVICE_TYPE,
                NsdManager.PROTOCOL_DNS_SD,
                discoveryListener)
        // end::start[]
    }

    private fun stopDiscovery() {
        if (nsdManager != null && discoveryListener != null) {
            nsdManager.stopServiceDiscovery(discoveryListener)
        }
    }

    private fun initializeDiscoveryListener() {
        discoveryListener = object : NsdManager.DiscoveryListener {

            override fun onDiscoveryStarted(regType: String) {
                Log.i(TAG, "Discovery started: " + regType)
            }

            // tag::found[]
            override fun onServiceFound(service: NsdServiceInfo) {
                Log.i(TAG, "Service found: " + service)
                if (service.serviceType != Constants.SERVICE_TYPE) {
                    Log.d(TAG, "Unknown Service Type: " + service.serviceType)
                } else if (service.serviceName == Constants.DEFAULT_SERVICE_NAME) {
                    nsdManager?.resolveService(service, resolveListener)
                }
            }
            // end::found[]

            override fun onServiceLost(service: NsdServiceInfo) {
                Log.e(TAG, "Service lost: " + service)
            }

            override fun onDiscoveryStopped(serviceType: String) {
                Log.i(TAG, "Discovery stopped: " + serviceType)
            }

            override fun onStartDiscoveryFailed(serviceType: String, errorCode: Int) {
                Log.e(TAG, "Start discovery failed; error code: " + errorCode)
                listener?.onServiceResolutionFailed()
            }

            override fun onStopDiscoveryFailed(serviceType: String, errorCode: Int) {
                Log.e(TAG, "Stop discovery failed; error code: " + errorCode)
                listener?.onServiceResolutionFailed()
            }
        }
    }

    private fun initializeResolveListener() {
        resolveListener = object : NsdManager.ResolveListener {

            override fun onResolveFailed(serviceInfo: NsdServiceInfo, errorCode: Int) {
                Log.e(TAG, "Resolve failed: " + errorCode)
                listener?.onServiceResolutionFailed()
            }

            // tag::resolved[]
            override fun onServiceResolved(serviceInfo: NsdServiceInfo) {
                Log.i(TAG, "Service resolved: " + serviceInfo)

                backingService = serviceInfo
                listener?.onServiceResolutionSucceeded()
            }
            // end::resolved[]
        }
    }

    companion object {
        private val TAG = "NativeZeroconfClient"
    }
}

package training.akosma.okhttp

import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException
import java.net.URL

// tag::apiconnector[]
internal class APIConnector {
    @Throws(IOException::class)
    fun getStringData(urlString: String): String? {
        val url = URL(urlString)
        val client = OkHttpClient()

        val request = Request.Builder()
                .url(url)
                .build()

        val response = client.newCall(request).execute()
        return response.body()?.string()
    }
}
// end::apiconnector[]

package training.akosma.notifications

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_list.*

import java.util.ArrayList

class ListFragment : Fragment() {
    private var list: List<String> = ArrayList()

    fun setItems(items: List<String>) {
        list = items
        if (isAdded) {
            listView.adapter = Adapter(list)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            list = arguments!!.getStringArrayList(ARG_LIST)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        listView.setHasFixedSize(true)
        listView.adapter = Adapter(list)
        listView.layoutManager = LinearLayoutManager(activity)
    }

    private inner class StringHolder internal constructor(v: View)
        : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var item: String? = null
        private val textView: TextView

        init {
            v.setOnClickListener(this)
            textView = v.findViewById(android.R.id.text1)
        }

        internal fun bind(value: String) {
            item = value
            textView.text = value
        }

        // tag::notify[]
        override fun onClick(view: View) {
            val intent = Intent(Constants.NOTIFICATION_NAME)
            intent.putExtra(Constants.DATA_KEY, item)
            val activity = activity
            val manager: LocalBroadcastManager
            manager = LocalBroadcastManager.getInstance(activity!!)
            manager.sendBroadcast(intent)
        }
        // end::notify[]
    }

    private inner class Adapter internal constructor(private val items: List<String>)
        : RecyclerView.Adapter<StringHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StringHolder {
            val inflater = LayoutInflater.from(activity)
            val view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false)
            return StringHolder(view)
        }

        override fun onBindViewHolder(holder: StringHolder, position: Int) {
            val item = items[position]
            holder.bind(item)
        }

        override fun getItemCount() = items.size
    }

    companion object {
        val ARG_LIST = "list"
    }
}

package training.akosma.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast

import java.util.ArrayList

class MainActivity : AppCompatActivity(), ListFragment.OnItemSelectionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val items = ArrayList<String>()
        for (i in 0..99) {
            items.add("Item " + i.toString())
        }

        val fragment = supportFragmentManager.findFragmentById(R.id.listFragment) as ListFragment
        fragment.setItems(items)
    }

    // tag::coordination[]
    override fun onItemSelected(value: String?) {
        Toast.makeText(this, value, Toast.LENGTH_SHORT).show()

        val detailFragment = supportFragmentManager.findFragmentById(R.id.itemFragment) as DetailFragment?
        if (detailFragment == null || !detailFragment.isInLayout) {
            val intent = Intent(this, DetailActivity::class.java)
            intent.putExtra(DetailFragment.PARAMETER, value)
            startActivity(intent)
        } else {
            detailFragment.update(value ?: "")
        }
    }
    // end::coordination[]
}

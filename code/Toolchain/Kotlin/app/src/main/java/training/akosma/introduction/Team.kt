package training.akosma.introduction

data class Team(var manager: Manager, var person: Person)
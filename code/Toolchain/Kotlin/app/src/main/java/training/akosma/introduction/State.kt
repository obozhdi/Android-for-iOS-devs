package training.akosma.introduction

// tag::enum[]
enum class State {
    On,
    Off
}
// end::enum[]

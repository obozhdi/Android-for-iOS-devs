package training.akosma.texttospeech

import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

import java.util.Locale

// tag::speech[]
class MainActivity : AppCompatActivity(), TextToSpeech.OnInitListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tts = TextToSpeech(this, this)
        tts.language = Locale.US

        speakButton.setOnClickListener {
            tts.speak(textField.text,
                    TextToSpeech.QUEUE_ADD,
                    null,
                    "test")
        }

        clearButton.setOnClickListener { textField.setText("") }
    }

    override fun onInit(i: Int) {
        speakButton.isEnabled = true
    }
}
// end::speech[]

package training.akosma.rxcalculator

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_calculator.*
import training.akosma.rxcalculator.engine.Calculator
import training.akosma.rxcalculator.engine.Digit
import training.akosma.rxcalculator.engine.operations.Addition
import training.akosma.rxcalculator.engine.operations.Division
import training.akosma.rxcalculator.engine.operations.Multiplication
import training.akosma.rxcalculator.engine.operations.Subtraction
import training.akosma.rxcalculator.engine.storage.Storage
import javax.inject.Inject

class CalculatorFragment : Fragment() {

    // tag::inject[]
    @Inject
    lateinit var calculator: Calculator

    @Inject
    lateinit var storage: Storage
    // end::inject[]

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // tag::build[]
        // Instead of hard-wiring dependencies:
        // calculator = IntegerCalculator()
        // storage = MemoryStorage()

        // We will use dependency injection:
        val app = activity?.application as CalculatorApplication
        app.calculatorComponent.inject(this)
        calculator.setStorage(storage)
        // end::build[]
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_calculator, container, false)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // tag::subscribe[]
        // Wire the observable
        calculator.observable.subscribe { value ->
            text_view_display.text = value?.toString()
        }
        // end::subscribe[]

        // Wire up all the components of the UI
        text_view_display.text = "0"
        button_delete.setOnClickListener { calculator.backspace() }
        button_ac.setOnClickListener { calculator.clear() }
        button_plus.setOnClickListener { calculator.enterOperation(Addition<Long>()) }
        button_minus.setOnClickListener { calculator.enterOperation(Subtraction<Long>()) }
        button_multiplication.setOnClickListener { calculator.enterOperation(Multiplication<Long>()) }
        button_division.setOnClickListener { calculator.enterOperation(Division<Long>()) }
        button_factorial.setOnClickListener { calculator.factorial() }
        button_0.setOnClickListener { calculator.enterDigit(Digit.Zero) }
        button_1.setOnClickListener { calculator.enterDigit(Digit.One) }
        button_2.setOnClickListener { calculator.enterDigit(Digit.Two) }
        button_3.setOnClickListener { calculator.enterDigit(Digit.Three) }
        button_4.setOnClickListener { calculator.enterDigit(Digit.Four) }
        button_5.setOnClickListener { calculator.enterDigit(Digit.Five) }
        button_6.setOnClickListener { calculator.enterDigit(Digit.Six) }
        button_7.setOnClickListener { calculator.enterDigit(Digit.Seven) }
        button_8.setOnClickListener { calculator.enterDigit(Digit.Eight) }
        button_9.setOnClickListener { calculator.enterDigit(Digit.Nine) }

        button_equal.setOnClickListener {
            calculator.execute()
            Log.i("CalculatorFragment", "Dump: " + calculator.toString())
        }
    }
}

package training.akosma.rxcalculator.engine

import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subjects.PublishSubject
import training.akosma.rxcalculator.engine.operations.Operation
import training.akosma.rxcalculator.engine.storage.Storage

internal class LongIntegerCalculator : Calculator {

    private var currentOperation: Operation<Long>? = null
    private var tempRegister: Long = 0
    private var register: Long = 0
    private var digitsCount: Long = 0
    private var storage: Storage? = null

    // tag::publish[]
    private val subject = PublishSubject.create<Long>()

    override val observable: Observable<Long>
        get() = subject
    // end::publish[]

    override fun setStorage(storage: Storage) {
        this.storage = storage
    }

    override fun enterDigit(digit: Digit) {
        if (digitsCount == 0L) {
            register = digit.value.toLong()
        } else {
            register = register * 10 + digit.value
        }
        digitsCount += 1

        // tag::notify[]
        subject.onNext(register)
        // end::notify[]
    }

    override fun enterOperation(op: Operation<*>) {
        tempRegister = register
        register = 0
        currentOperation = op as Operation<Long>?
        digitsCount = 0
        subject.onNext(register)
    }

    override fun factorial() {
        val input = register
        Observable.fromCallable {
            var fact: Long = 1
            if (input > 0) {
                for (i in 1..input) {
                    fact *= i
                }
            }
            fact
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { fact ->
                    register = fact!!
                    subject.onNext(register)
                }
    }

    override fun clear() {
        if (currentOperation == null) {
            register = 0
        }
        currentOperation = null
        digitsCount = 0
        tempRegister = 0
        subject.onNext(register)
    }

    override fun execute() {
        if (currentOperation != null) {
            val result = currentOperation!!.execute(tempRegister, register)
            if (storage != null) {
                storage!!.addOperation(currentOperation!!.getString(tempRegister, register))
            }
            register = result.toInt().toLong()
            clear()
        } else {
            register = 0
            clear()
        }
    }

    override fun backspace() {
        register /= 10
        subject.onNext(register)
    }

    override fun toString(): String {
        return if (storage != null) {
            storage!!.toString()
        } else "Integer Calculator, no storage"
    }
}

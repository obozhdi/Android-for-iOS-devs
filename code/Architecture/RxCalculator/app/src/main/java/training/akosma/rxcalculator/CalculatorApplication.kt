package training.akosma.rxcalculator

import android.app.Application

import training.akosma.rxcalculator.engine.CalculatorComponent
import training.akosma.rxcalculator.engine.DaggerCalculatorComponent

class CalculatorApplication : Application() {

    lateinit var calculatorComponent: CalculatorComponent
        private set

    override fun onCreate() {
        super.onCreate()

        calculatorComponent = DaggerCalculatorComponent.builder().build()
    }
}

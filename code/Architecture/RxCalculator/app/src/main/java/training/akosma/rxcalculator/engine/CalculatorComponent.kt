package training.akosma.rxcalculator.engine

import dagger.Component
import training.akosma.rxcalculator.CalculatorFragment

// tag::component[]
@Component(modules = arrayOf(CalculatorModule::class))
interface CalculatorComponent {
    fun inject(fragment: CalculatorFragment)
}
// end::component[]

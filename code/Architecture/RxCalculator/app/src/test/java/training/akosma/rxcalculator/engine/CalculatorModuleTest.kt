package training.akosma.rxcalculator.engine

import org.junit.Assert.assertEquals
import org.junit.Test

class CalculatorModuleTest {
    @Test
    @Throws(Exception::class)
    fun provideCalculator() {
        val module = CalculatorModule()
        val calc = module.provideCalculator()
        assertEquals(calc.javaClass.getName(), "training.akosma.rxcalculator.engine.LongIntegerCalculator")
    }

    @Test
    @Throws(Exception::class)
    fun provideStorage() {
        val module = CalculatorModule()
        val storage = module.provideStorage()
        assertEquals(storage.javaClass.getName(), "training.akosma.rxcalculator.engine.storage.MemoryStorage")
    }

}
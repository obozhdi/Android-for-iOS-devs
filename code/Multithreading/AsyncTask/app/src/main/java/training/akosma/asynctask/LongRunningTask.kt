package training.akosma.asynctask

import android.os.AsyncTask

import java.util.concurrent.TimeUnit

// tag::definition[]
class LongRunningTask(private val listener: LongRunningTaskListener)
    : AsyncTask<Void, Int, Boolean>() {
// end::definition[]

    // tag::doinback[]
    override fun doInBackground(vararg voids: Void): Boolean? {
        var count = -1
        while (count < 10 && !isCancelled) { // <1>
            count += 1
            val progress = count * 10
            try {
                TimeUnit.SECONDS.sleep(1)
            } catch (e: InterruptedException) { /* Do nothing */ }

            publishProgress(progress)        // <2>
        }
        return true
    }
    // end::doinback[]

    // tag::pre[]
    override fun onPreExecute() {
        listener.started(this)
    }
    // end::pre[]

    // tag::prog[]
    override fun onProgressUpdate(vararg values: Int?) {
        listener.reportProgress(this, values.get(0) ?: 0)
    }
    // end::prog[]

    // tag::cancel[]
    override fun onCancelled(bool: Boolean?) {
        listener.cancelled(this)
    }
    // end::cancel[]

    // tag::post[]
    override fun onPostExecute(bool: Boolean?) {
        listener.finished(this)
    }
    // end::post[]
}

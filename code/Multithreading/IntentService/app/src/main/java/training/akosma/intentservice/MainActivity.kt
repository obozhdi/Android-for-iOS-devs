package training.akosma.intentservice

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import training.akosma.intentservice.MyIntentService.Companion.PROGRESS_BROADCAST
import java.util.*

class MainActivity : AppCompatActivity() {
    private var taskID1 = INVALID_TASK_ID
    private var taskID2 = INVALID_TASK_ID

    private val startReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val taskID = intent.getIntExtra(MyIntentService.TASK_ID, 0)
            started(taskID)
        }
    }

    // tag::progress1[]
    private val progressReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val progress = intent.getIntExtra(MyIntentService.PROGRESS, 0)
            val taskID = intent.getIntExtra(MyIntentService.TASK_ID, 0)
            reportProgress(taskID, progress)
        }
    }
    // end::progress1[]

    private val finishedReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val taskID = intent.getIntExtra(MyIntentService.TASK_ID, 0)
            finished(taskID)
        }
    }

    private val cancelledReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val taskID = intent.getIntExtra(MyIntentService.TASK_ID, 0)
            cancelled(taskID)
        }
    }

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        setContentView(R.layout.activity_main)

        if (bundle != null) {
            taskID1 = bundle.getInt(TASK_ID_1)
            taskID2 = bundle.getInt(TASK_ID_2)
            textView1.text = bundle.getString(TEXT_VIEW_1)
            textView2.text = bundle.getString(TEXT_VIEW_2)
            executeButton.isEnabled = bundle.getBoolean(EXEC_BTN_ENABLED)
            cancelButton.isEnabled = bundle.getBoolean(CANCEL_BTN_ENABLED)
        }

        // tag::progress2[]
        val manager = LocalBroadcastManager.getInstance(this)
        val filter = IntentFilter(PROGRESS_BROADCAST)
        manager.registerReceiver(progressReceiver, filter)
        // end::progress2[]

        val startFilter = IntentFilter(MyIntentService.START_BROADCAST)
        manager.registerReceiver(startReceiver, startFilter)

        val finishedFilter = IntentFilter(MyIntentService.FINISH_BROADCAST)
        manager.registerReceiver(finishedReceiver, finishedFilter)

        val cancelledFilter = IntentFilter(MyIntentService.CANCEL_BROADCAST)
        manager.registerReceiver(cancelledReceiver, cancelledFilter)

        executeButton.setOnClickListener {
            taskID1 = 0
            taskID2 = 1

            // tag::start[]
            val intent = Intent(this, MyIntentService::class.java)
            intent.action = MyIntentService.ACTION_START
            startService(intent)
            // end::start[]
        }

        cancelButton.setOnClickListener {
            // tag::cancel[]
            MyIntentService.cancelled = true
            // end::cancel[]
        }
    }

    override fun onSaveInstanceState(bundle: Bundle?) {
        super.onSaveInstanceState(bundle)
        if (bundle != null) {
            bundle.putInt(TASK_ID_1, taskID1)
            bundle.putInt(TASK_ID_2, taskID2)
            bundle.putString(TEXT_VIEW_1, textView1.text.toString())
            bundle.putString(TEXT_VIEW_2, textView2.text.toString())
            bundle.putBoolean(EXEC_BTN_ENABLED, executeButton.isEnabled)
            bundle.putBoolean(CANCEL_BTN_ENABLED, cancelButton.isEnabled)
        }
    }

    fun started(taskID: Int) {
        if (taskID == taskID1) {
            progressBar1.progress = 0
            textView1.setText(R.string.processing)
        } else if (taskID == taskID2) {
            progressBar2.progress = 0
            textView2.setText(R.string.processing)
        }
        executeButton.isEnabled = false
        cancelButton.isEnabled = true
    }

    fun finished(taskID: Int) {
        if (taskID == taskID1) {
            textView1.setText(R.string.done)
            taskID1 = INVALID_TASK_ID
        } else if (taskID == taskID2) {
            textView2.setText(R.string.done)
            taskID2 = INVALID_TASK_ID
        }
        if (taskID1 == INVALID_TASK_ID && taskID2 == INVALID_TASK_ID) {
            executeButton.isEnabled = true
            cancelButton.isEnabled = false
        }
    }

    fun cancelled(taskID: Int) {
        if (taskID == taskID1) {
            textView1.setText(R.string.cancelled)
            taskID1 = INVALID_TASK_ID
        } else if (taskID == taskID2) {
            textView2.setText(R.string.cancelled)
            taskID2 = INVALID_TASK_ID
        }
        if (taskID1 == INVALID_TASK_ID && taskID2 == INVALID_TASK_ID) {
            executeButton.isEnabled = true
            cancelButton.isEnabled = false
        }
    }

    fun reportProgress(taskID: Int, progress: Int) {
        val template = getString(R.string.progress)
        val text = String.format(Locale.ENGLISH, template, progress)
        if (taskID == taskID1) {
            textView1.text = text
            progressBar1.progress = progress
        } else if (taskID == taskID2) {
            textView2.text = text
            progressBar2.progress = progress
        }
    }

    companion object {
        private val TASK_ID_1 = "TASK_ID_1"
        private val TASK_ID_2 = "TASK_ID_2"
        private val TEXT_VIEW_1 = "TEXT_VIEW_1"
        private val TEXT_VIEW_2 = "TEXT_VIEW_2"
        private val EXEC_BTN_ENABLED = "EXEC_BTN_ENABLED"
        private val CANCEL_BTN_ENABLED = "CANCEL_BTN_ENABLED"
        private val INVALID_TASK_ID = -1
    }
}

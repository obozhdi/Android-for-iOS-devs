package training.akosma.intentservice

import android.app.IntentService
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager

class MyIntentService : IntentService("MyIntentService"), LongRunningTaskListener {

    // tag::onhandle[]
    override fun onHandleIntent(intent: Intent?) {
        if (intent != null) {
            val action = intent.action
            if (ACTION_START == action) {
                MyIntentService.cancelled = false

                val task1 = LongRunningTask(this)
                task1.taskID = 0
                task1.execute()
                val task2 = LongRunningTask(this)
                task2.taskID = 1
                task2.execute()
            }
        }
    }
    // end::onhandle[]

    override fun started(task: LongRunningTask) {
        val broadcast = Intent(START_BROADCAST)
        broadcast.putExtra(TASK_ID, task.taskID)

        val manager: LocalBroadcastManager
        manager = LocalBroadcastManager.getInstance(applicationContext)
        manager.sendBroadcast(broadcast)
    }

    override fun reportProgress(task: LongRunningTask, progress: Int) {
        val broadcast = Intent(PROGRESS_BROADCAST)
        broadcast.putExtra(TASK_ID, task.taskID)
        broadcast.putExtra(PROGRESS, progress)

        val manager: LocalBroadcastManager
        manager = LocalBroadcastManager.getInstance(applicationContext)
        manager.sendBroadcast(broadcast)
    }

    override fun finished(task: LongRunningTask) {
        val broadcast = Intent(FINISH_BROADCAST)
        broadcast.putExtra(TASK_ID, task.taskID)

        val manager: LocalBroadcastManager
        manager = LocalBroadcastManager.getInstance(applicationContext)
        manager.sendBroadcast(broadcast)
    }

    override fun cancelled(task: LongRunningTask) {
        val broadcast = Intent(CANCEL_BROADCAST)
        broadcast.putExtra(TASK_ID, task.taskID)

        val manager: LocalBroadcastManager
        manager = LocalBroadcastManager.getInstance(applicationContext)
        manager.sendBroadcast(broadcast)
    }

    companion object {
        val ACTION_START = "training.akosma.intentservice.action.START"
        val PROGRESS_BROADCAST = "PROGRESS_BROADCAST"
        val START_BROADCAST = "START_BROADCAST"
        val FINISH_BROADCAST = "FINISH_BROADCAST"
        val CANCEL_BROADCAST = "CANCEL_BROADCAST"
        val PROGRESS = "PROGRESS"
        val TASK_ID = "TASK_ID"

        // https://stackoverflow.com/a/22967394/133764
        // tag::cancel[]
        @Volatile
        var cancelled = false
        // end::cancel[]
    }
}

package training.akosma.intentservice

import java.util.concurrent.TimeUnit

class LongRunningTask(private val listener: LongRunningTaskListener) {
    var taskID = -1

    // tag::check[]
    private val isCancelled: Boolean
        get() = MyIntentService.cancelled
    // end::check[]

    fun execute() {
        listener.started(this)
        var count = -1
        while (count < 10 && !isCancelled) {
            count += 1
            val progress = count * 10
            try {
                TimeUnit.SECONDS.sleep(1)
            } catch (e: InterruptedException) { /* Do nothing */
            }

            listener.reportProgress(this, progress)
        }
        if (isCancelled) {
            listener.cancelled(this)
        } else {
            listener.finished(this)
        }
    }
}

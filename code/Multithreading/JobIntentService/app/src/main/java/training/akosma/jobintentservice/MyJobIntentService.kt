package training.akosma.jobintentservice

import android.content.Intent
import android.support.v4.app.JobIntentService

import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

import java.util.ArrayList
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadFactory
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

class MyJobIntentService : JobIntentService(), LongRunningTaskListener {
    private val executor: ThreadPoolExecutor
    private val tasks = ArrayList<LongRunningTask>()

    init {
        EventBus.getDefault().register(this)

        val factory = ThreadFactory { runnable -> Thread(runnable) }

        val coresCount = Runtime.getRuntime().availableProcessors()
        executor = ThreadPoolExecutor(coresCount * 2,
                coresCount * 2,
                60L,
                TimeUnit.SECONDS,
                LinkedBlockingQueue(),
                factory)
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    // tag::handle[]
    override fun onHandleWork(intent: Intent) {
        val action = intent.action
        val taskID = intent.getIntExtra(TASK_ID, 0)
        if (ACTION_START == action) {
            val task = LongRunningTask(this)
            task.taskID = taskID
            tasks.add(task)
            executor.execute(task)
        }
    }
    // end::handle[]

    override fun started(task: LongRunningTask) {
        val event = StartEvent(task.taskID)
        EventBus.getDefault().post(event)
    }

    override fun reportProgress(task: LongRunningTask, progress: Int) {
        val event = ProgressEvent(task.taskID, progress)
        EventBus.getDefault().post(event)
    }

    override fun finished(task: LongRunningTask) {
        val event = FinishEvent(task.taskID)
        EventBus.getDefault().post(event)
    }

    override fun cancelled(task: LongRunningTask) {
        val event = CancelledEvent(task.taskID)
        EventBus.getDefault().post(event)
    }

    @Subscribe
    fun onCancelActionEvent(event: MyJobIntentService.CancelActionEvent) {
        for (task in tasks) {
            if (event.taskID == task.taskID) {
                task.cancel()
            }
        }
    }

    class ProgressEvent(val taskID: Int, val progress: Int)

    class StartEvent(val taskID: Int)

    class CancelledEvent(val taskID: Int)

    class CancelActionEvent(val taskID: Int)

    class FinishEvent(val taskID: Int)

    companion object {
        val ACTION_START = "training.akosma.jobintentservice.action.START"
        val TASK_ID = "TASK_ID"
    }
}

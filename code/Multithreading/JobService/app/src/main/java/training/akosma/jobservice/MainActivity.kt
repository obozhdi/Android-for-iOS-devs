package training.akosma.jobservice

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*

class MainActivity : AppCompatActivity() {
    private var taskID1 = INVALID_TASK_ID
    private var taskID2 = INVALID_TASK_ID

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        setContentView(R.layout.activity_main)

        if (bundle != null) {
            taskID1 = bundle.getInt(TASK_ID_1)
            taskID2 = bundle.getInt(TASK_ID_2)
            textView1.text = bundle.getString(TEXT_VIEW_1)
            textView2.text = bundle.getString(TEXT_VIEW_2)
            executeButton.isEnabled = bundle.getBoolean(EXEC_BTN_ENABLED)
            cancelButton.isEnabled = bundle.getBoolean(CANCEL_BTN_ENABLED)
        }

        executeButton.setOnClickListener {
            // tag::launch[]
            taskID1 = 0
            taskID2 = 1
            val component = ComponentName(this,
                    MyJobService::class.java)

            val builder1 = JobInfo.Builder(taskID1, component)
            builder1.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY).build()
            val info1 = builder1.build()

            val builder2 = JobInfo.Builder(taskID2, component)
            builder2.setRequiresDeviceIdle(false)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                builder2.setRequiresBatteryNotLow(true)
            }

            val info2 = builder2.build()

            val sched = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
            sched.schedule(info1)
            sched.schedule(info2)
            // end::launch[]
        }

        cancelButton.setOnClickListener {
            // tag::cancel[]
            val sched = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler

            sched.cancel(taskID1)
            sched.cancel(taskID2)
            // end::cancel[]
        }
    }

    // tag::bus[]
    public override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    public override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }
    // end::bus[]

    override fun onSaveInstanceState(bundle: Bundle?) {
        super.onSaveInstanceState(bundle)
        if (bundle != null) {
            bundle.putInt(TASK_ID_1, taskID1)
            bundle.putInt(TASK_ID_2, taskID2)
            bundle.putString(TEXT_VIEW_1, textView1.text.toString())
            bundle.putString(TEXT_VIEW_2, textView2.text.toString())
            bundle.putBoolean(EXEC_BTN_ENABLED, executeButton.isEnabled)
            bundle.putBoolean(CANCEL_BTN_ENABLED, cancelButton.isEnabled)
        }
    }

    // tag::subscribe[]
    @Subscribe
    fun onStartEvent(event: MyJobService.StartEvent) {
        started(event.taskID)
    }
    // end::subscribe[]

    @Subscribe
    fun onProgressEvent(event: MyJobService.ProgressEvent) {
        reportProgress(event.taskID, event.progress)
    }

    @Subscribe
    fun onFinishEvent(event: MyJobService.FinishEvent) {
        finished(event.taskID)
    }

    @Subscribe
    fun onCancelEvent(event: MyJobService.CancelEvent) {
        cancelled(event.taskID)
    }

    private fun started(taskID: Int) {
        if (taskID == taskID1) {
            progressBar1.progress = 0
            textView1.setText(R.string.processing)
        } else if (taskID == taskID2) {
            progressBar2.progress = 0
            textView2.setText(R.string.processing)
        }
        executeButton.isEnabled = false
        cancelButton.isEnabled = true
    }

    private fun finished(taskID: Int) {
        if (taskID == taskID1) {
            textView1.setText(R.string.done)
            taskID1 = INVALID_TASK_ID
        } else if (taskID == taskID2) {
            textView2.setText(R.string.done)
            taskID2 = INVALID_TASK_ID
        }
        if (taskID1 == INVALID_TASK_ID && taskID2 == INVALID_TASK_ID) {
            executeButton.isEnabled = true
            cancelButton.isEnabled = false
        }
    }

    private fun cancelled(taskID: Int) {
        if (taskID == taskID1) {
            textView1.setText(R.string.cancelled)
            taskID1 = INVALID_TASK_ID
        } else if (taskID == taskID2) {
            textView2.setText(R.string.cancelled)
            taskID2 = INVALID_TASK_ID
        }
        if (taskID1 == INVALID_TASK_ID && taskID2 == INVALID_TASK_ID) {
            executeButton.isEnabled = true
            cancelButton.isEnabled = false
        }
    }

    private fun reportProgress(taskID: Int, progress: Int) {
        val template = getString(R.string.progress)
        val text = String.format(Locale.ENGLISH, template, progress)
        if (taskID == taskID1) {
            textView1.text = text
            progressBar1.progress = progress
        } else if (taskID == taskID2) {
            textView2.text = text
            progressBar2.progress = progress
        }
    }

    companion object {
        private val TASK_ID_1 = "TASK_ID_1"
        private val TASK_ID_2 = "TASK_ID_2"
        private val TEXT_VIEW_1 = "TEXT_VIEW_1"
        private val TEXT_VIEW_2 = "TEXT_VIEW_2"
        private val EXEC_BTN_ENABLED = "EXEC_BTN_ENABLED"
        private val CANCEL_BTN_ENABLED = "CANCEL_BTN_ENABLED"
        private val INVALID_TASK_ID = -1
    }
}

package training.akosma.handlerthread

import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), LongRunningTaskListener {
    private var handlerThread: HandlerThread? = null
    private var handlerObject: Handler? = null
    private var task1: LongRunningTask? = null
    private var task2: LongRunningTask? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // tag::setup[]
        handlerThread = HandlerThread("MyHandlerThread")
        handlerThread?.start()
        handlerObject = Handler(handlerThread?.looper)
        // end::setup[]

        dispatchCheckBox.setOnClickListener {
            checkBox.isEnabled = !dispatchCheckBox.isChecked
        }

        executeButton.setOnClickListener {
            if (dispatchCheckBox.isChecked) {
                executeButton.isEnabled = false

                // tag::queue[]
                val queue1 = DispatchQueue("Worker1")
                queue1.async {
                    for (i in 1..10) {
                        DispatchQueue.main.async {
                            textView1.text = "Step $i"
                            progressBar1.progress = i * 10
                        }
                        TimeUnit.SECONDS.sleep(1)
                    }

                    DispatchQueue.main.async {
                        executeButton.isEnabled = true
                    }
                }
                // end::queue[]

                val queue2 = DispatchQueue("Worker2")
                queue2.async {
                    for (i in 1..10) {
                        DispatchQueue.main.async {
                            textView2.text = "Step $i"
                            progressBar2.progress = i * 10
                        }
                        TimeUnit.SECONDS.sleep(1)
                    }

                    DispatchQueue.main.async {
                        executeButton.isEnabled = true
                    }
                }

            } else {
                if (checkBox.isChecked) {
                    // tag::delayed[]
                    task1 = LongRunningTask(this)
                    handlerObject?.postDelayed(task1, 20000)
                    // end::delayed[]

                    task2 = LongRunningTask(this)
                    handlerObject?.postDelayed(task2, 2000)
                } else {
                    // tag::post[]
                    task1 = LongRunningTask(this)
                    handlerObject?.post(task1)
                    // end::post[]

                    task2 = LongRunningTask(this)
                    handlerObject?.post(task2)
                }
            }
        }

        cancelButton.setOnClickListener {
            task1?.cancel()
            task2?.cancel()
        }
    }

    override fun started(task: LongRunningTask) {
        if (task === task1) {
            progressBar1.progress = 0
            textView1.setText(R.string.processing)
        } else if (task === task2) {
            progressBar2.progress = 0
            textView2.setText(R.string.processing)
        }
        executeButton.isEnabled = false
        cancelButton.isEnabled = true
    }

    override fun finished(task: LongRunningTask) {
        if (task === task1) {
            textView1.setText(R.string.done)
            task1 = null
        } else if (task === task2) {
            textView2.setText(R.string.done)
            task2 = null
        }
        if (task1 == null && task2 == null) {
            executeButton.isEnabled = true
            cancelButton.isEnabled = false
        }
    }

    override fun cancelled(task: LongRunningTask) {
        if (task === task1) {
            textView1.setText(R.string.cancelled)
            handlerObject?.removeCallbacks(task1)
            task1 = null
        } else if (task === task2) {
            textView2.setText(R.string.cancelled)
            handlerObject?.removeCallbacks(task2)
            task2 = null
        }
        if (task1 == null && task2 == null) {
            executeButton.isEnabled = true
            cancelButton.isEnabled = false
        }
    }

    override fun reportProgress(task: LongRunningTask, progress: Int) {
        val template = getString(R.string.progress)
        val text = String.format(Locale.ENGLISH, template, progress)
        if (task === task1) {
            textView1.text = text
            progressBar1.progress = progress
        } else if (task === task2) {
            textView2.text = text
            progressBar2.progress = progress
        }
    }
}

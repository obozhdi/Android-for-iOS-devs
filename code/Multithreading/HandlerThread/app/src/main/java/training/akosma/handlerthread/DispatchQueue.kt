package training.akosma.handlerthread

import android.os.Handler
import android.os.HandlerThread
import android.os.Looper

// tag::queue[]
class DispatchQueue(private val label: String = "main") {
    val handlerThread: HandlerThread?
    val handler: Handler

    init {
        if (label == "main") {
            handlerThread = null
            handler = Handler(Looper.getMainLooper())
        } else {
            handlerThread = HandlerThread(label)
            handlerThread.start()
            handler = Handler(handlerThread.looper)
        }
    }

    fun async(runnable: Runnable) = handler.post(runnable)

    fun async(block: () -> (Unit)) = handler.post(block)

    fun asyncAfter(milliseconds: Long, function: () -> (Unit)) {
        handler.postDelayed(function, milliseconds)
    }

    fun asyncAfter(milliseconds: Long, runnable: Runnable) {
        handler.postDelayed(runnable, milliseconds)
    }

    override fun toString() = label

    companion object {
        val main = DispatchQueue()

        private val global = DispatchQueue("global")
        fun global() = global
    }
}
// end::queue[]

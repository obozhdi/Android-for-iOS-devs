package training.akosma.rxcalculator.engine.operations

class Division<T : Number> : Operation<T> {
    override fun execute(op1: T, op2: T): Number {
        require(op2.toDouble() != 0.0) { "The second operator must be bigger than zero!" }

        return op1.toDouble() / op2.toDouble()
    }

    override fun getString(op1: T, op2: T): String {
        val sb = StringBuilder()
        sb.append(op1.toDouble())
                .append(" / ")
                .append(op2.toDouble())
                .append(" = ")
                .append(op1.toDouble() / op2.toDouble())
        return sb.toString()
    }
}

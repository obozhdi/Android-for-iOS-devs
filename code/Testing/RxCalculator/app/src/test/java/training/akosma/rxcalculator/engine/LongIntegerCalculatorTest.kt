package training.akosma.rxcalculator.engine

import org.junit.Before
import org.junit.Test

import rx.Observer
import training.akosma.rxcalculator.engine.operations.Addition
import training.akosma.rxcalculator.engine.operations.Operation
import training.akosma.rxcalculator.engine.storage.MemoryStorage
import training.akosma.rxcalculator.engine.storage.Storage

import org.junit.Assert.assertEquals

class LongIntegerCalculatorTest {

    private var mCalculator: Calculator? = null
    private var mLong: Long? = null

    @Before
    @Throws(Exception::class)
    fun setUp() {
        mCalculator = LongIntegerCalculator()
        mCalculator!!.observable.subscribe { value ->
            mLong = value as Long?
        }
    }

    @Test
    @Throws(Exception::class)
    fun enterDigit() {
        mCalculator!!.enterDigit(Digit.One)
        assertEquals(1, mLong!!.toLong())
        mCalculator!!.enterDigit(Digit.Two)
        assertEquals(12, mLong!!.toLong())
        mCalculator!!.enterDigit(Digit.Three)
        assertEquals(123, mLong!!.toLong())
        mCalculator!!.enterDigit(Digit.Four)
        assertEquals(1234, mLong!!.toLong())
    }

    @Test
    @Throws(Exception::class)
    fun enterOperation() {
        mCalculator!!.enterDigit(Digit.One)
        mCalculator!!.enterDigit(Digit.Two)
        mCalculator!!.enterDigit(Digit.Three)
        assertEquals(123, mLong!!.toLong())

        val op = Addition<Float>()
        mCalculator!!.enterOperation(op)
        assertEquals(0, mLong!!.toLong())
    }

    @Test
    @Throws(Exception::class)
    fun clear() {
        mCalculator!!.enterDigit(Digit.One)
        mCalculator!!.enterDigit(Digit.Two)
        mCalculator!!.enterDigit(Digit.Three)
        assertEquals(123, mLong!!.toLong())

        mCalculator!!.clear()
        assertEquals(0, mLong!!.toLong())
    }

    @Test
    @Throws(Exception::class)
    fun execute() {
        mCalculator!!.enterDigit(Digit.One)
        mCalculator!!.enterDigit(Digit.Two)
        mCalculator!!.enterDigit(Digit.Three)
        assertEquals(123, mLong!!.toLong())

        val op = Addition<Float>()
        mCalculator!!.enterOperation(op)
        assertEquals(0, mLong!!.toLong())

        mCalculator!!.enterDigit(Digit.Four)
        mCalculator!!.enterDigit(Digit.Five)
        mCalculator!!.enterDigit(Digit.Six)
        assertEquals(456, mLong!!.toLong())

        mCalculator!!.execute()
        assertEquals(579, mLong!!.toLong())
    }

    @Test
    @Throws(Exception::class)
    fun backspace() {
        mCalculator!!.enterDigit(Digit.Seven)
        mCalculator!!.enterDigit(Digit.Eight)
        mCalculator!!.enterDigit(Digit.Nine)
        mCalculator!!.enterDigit(Digit.Zero)
        assertEquals(7890, mLong!!.toLong())

        mCalculator!!.backspace()
        assertEquals(789, mLong!!.toLong())
    }

    @Test
    @Throws(Exception::class)
    fun storage() {
        val storage = MemoryStorage()
        mCalculator!!.setStorage(storage)
        execute()
        val value = mCalculator!!.toString()
        assertEquals("[123.0 + 456.0 = 579.0]", value)
        assertEquals(1, storage.operations.size.toLong())
    }

}
package training.akosma.screensize

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        relativeLayout.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LOW_PROFILE or
                        View.SYSTEM_UI_FLAG_FULLSCREEN or
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION

        // tag::absolute[]
        val width = 300
        val height = 300

        val display = windowManager.defaultDisplay
        val metrics = DisplayMetrics()
        display.getRealMetrics(metrics)
        val maxX = metrics.widthPixels
        val maxY = metrics.heightPixels
        val density = metrics.densityDpi

        sizeLabel.text = """
            Screen: $maxX x $maxY px
            Density: $density dpi
            """.trimIndent()

        val topLeftTextView = TextView(this)
        topLeftTextView.text = "Top Left"
        val topLeft = RelativeLayout.LayoutParams(width, height)
        topLeftTextView.setBackgroundColor(Color.BLUE)
        topLeftTextView.setTextColor(Color.WHITE)
        topLeftTextView.setTypeface(null, Typeface.BOLD_ITALIC)
        topLeftTextView.textSize = 20f
        relativeLayout.addView(topLeftTextView, topLeft)
        // end::absolute[]

        val topRightTextView = TextView(this)
        topRightTextView.text = "Top Right"
        val topRight = RelativeLayout.LayoutParams(width, height)
        topRight.leftMargin = maxX - width
        topRightTextView.setBackgroundColor(Color.RED)
        topRightTextView.setTextColor(Color.WHITE)
        topRightTextView.setTypeface(null, Typeface.BOLD_ITALIC)
        topRightTextView.textSize = 20f
        topRightTextView.textAlignment = View.TEXT_ALIGNMENT_TEXT_END
        relativeLayout.addView(topRightTextView, topRight)

        val bottomLeftTextView = TextView(this)
        bottomLeftTextView.text = "Bottom Left"
        val bottomLeft = RelativeLayout.LayoutParams(width, height)
        bottomLeft.topMargin = maxY - height
        bottomLeftTextView.setBackgroundColor(Color.GREEN)
        bottomLeftTextView.setTypeface(null, Typeface.BOLD_ITALIC)
        bottomLeftTextView.textSize = 20f
        bottomLeftTextView.gravity = Gravity.BOTTOM
        relativeLayout.addView(bottomLeftTextView, bottomLeft)

        val bottomRightTextView = TextView(this)
        bottomRightTextView.text = "Bottom Right"
        val bottomRight = RelativeLayout.LayoutParams(width, height)
        bottomRight.leftMargin = maxX - width
        bottomRight.topMargin = maxY - height
        bottomRightTextView.setBackgroundColor(Color.YELLOW)
        bottomRightTextView.setTypeface(null, Typeface.BOLD_ITALIC)
        bottomRightTextView.textSize = 20f
        bottomRightTextView.textAlignment = View.TEXT_ALIGNMENT_TEXT_END
        bottomRightTextView.gravity = Gravity.BOTTOM
        relativeLayout.addView(bottomRightTextView, bottomRight)
    }
}

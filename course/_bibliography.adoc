[[bibliography]]
= Bibliography

This is the list of books and articles used during the preparation of this book.

.Books

- [[[Bloch]]] Joshua Bloch. https://www.amazon.de/Effective-Java-Programming-Language-Guide-ebook/dp/B00B8V09HY[Effective Java: A Programming Language Guide – Second Edition].  Addison-Wesley Professional. 2008. ISBN 978-0321356680.
- [[[Darwin]]] Ian F. Darwin. http://shop.oreilly.com/product/0636920010241.do[Android Cookbook – Problems and Solutions for Android Developers]. O'Reilly Media. 2012. ISBN 978-1-4493-8841-6.
- [[[Kousen]]] Ken Kousen. http://shop.oreilly.com/product/0636920032656.do[Gradle Recipes for Android]. O'Reilly Media. 2016. ISBN 978-1-4919-4702-9.
- [[[Leiva]]] Antonio Leiva. https://leanpub.com/kotlin-for-android-developers[Kotlin for Android Developers]. Leanpub. 2016.
- [[[Mcconnell]]] Steve McConnell. http://www.cc2e.com/[Code Complete, Second Edition]. Microsoft Press. 2004. ISBN 0-7356-1967-0.
- [[[Mednieks]]] Zigurd Mednieks, Laird Dornin, G. Blake Meike, and Masumi Nakamura. http://shop.oreilly.com/product/0636920023005.do[Programming Android 2nd Edition]. O'Reilly Media. 2012. ISBN 978-1-449-31664-8.
- [[[Phillips]]] Bill Phillips, Chris Stewart, Brian Hardy, Kristin Marsicano. https://www.bignerdranch.com/we-write/android-programming/[Android Programming: The Big Nerd Ranch Guide, 2nd Edition]. Big Nerd Ranch Guides. 2015. ISBN 978-0134171456.
- [[[Sommerville]]] Ian Sommerville. http://www.software-engin.com/books[Software Engineering, Eighth Edition]. Addison Wesley. 2007. ISBN 978-0-321-31379-9.

.Websites and Blog Articles

- [[[Apple]]] Apple. https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/zzSummaryOfTheGrammar.html#//apple_ref/doc/uid/TP40014097-CH38-ID458[Swift 4.0.3 Summary of the Grammar]. Published in developer.apple.com, December 2017.
- [[[Barnes]]] Stephen Barnes. https://www.objc.io/issues/11-android/android_101_for_ios_developers/[Android 101 for iOS Developers]. Published in objc.io, April 2014.
- [[[Cejas]]] Fernando Cejas. http://fernandocejas.com/2014/09/03/architecting-android-the-clean-way/[Architecting Android…The clean way?]. Published in fernandocejas.com, September 2014.
- [[[Farina]]] Nick Farina. http://nfarina.com/post/8239634061/ios-to-android[An iOS Developer Takes on Android]. Published in nfarina.com, August 2011.
- [[[Hanning]]] Thomas Hanning. http://www.thomashanning.com/android-development-ios-developers-overview[Android Development For iOS Developers – An Overview]. Published in thomashanning.com, March 24th, 2016.
- [[[Hoare]]] Sir Charles Anthony Richard Hoare. https://www.infoq.com/presentations/Null-References-The-Billion-Dollar-Mistake-Tony-Hoare[Null References: The Billion Dollar Mistake]. Published in infoq.com, August 25th, 2009.
- [[[iA]]] Information Architects. https://ia.net/writer/updates/starting-out-on-android[Starting Out on Android]. Published in ia.net, February 25th, 2015.
- [[[JetBrains]]] JetBrains. https://kotlinlang.org/docs/reference/grammar.html[Kotlin Grammar Reference]. Published in kotlinlang.org.
- [[[Kerimbaev]]] Bolot Kerimbaev. https://www.bignerdranch.com/blog/constraintlayout-vs-auto-layout-how-do-they-compare/[ConstraintLayout vs Auto Layout: How Do They Compare?]. Published in bignerdranch.com, October 7th, 2016.
- [[[Kriplaney]]] Vikdram Kriplaney & Sebastián Vieira. https://realm.io/news/appbuilders-kriplaney-vieira-ios-android-faceoff/[Faceoff: Android vs. iOS]. Published in realm.io, June 27th, 2016.
- [[[Leiva2]]] Antonio Leiva. http://antonioleiva.com/recyclerview/[RecyclerView in Android: The Basics]. Published in antonioleiva.com, June 29th, 2014.
- [[[Mottier]]] Cyril Mottier. http://cyrilmottier.com/2014/09/25/deep-dive-into-android-state-restoration/[Deep Dive Into Android State Restoration]. Published in cyrilmottier.com, September 25th, 2014.
- [[[Nakhimovich]]] Mike Nakhimovich. http://open.blogs.nytimes.com/2016/02/11/improving-startup-time-in-the-nytimes-android-app/[Improving Startup Time in the NYTimes Android App]. Published in open.blogs.nytimes.com, February 11th, 2016.
- [[[Poehls]]] Marcus Poehls. https://futurestud.io/tutorials/retrofit-getting-started-and-android-client[Retrofit Tutorial Series]. Published in futurestud.io, December 1st, 2014.
- [[[Puthraya]]] Thejaswi Puthraya. http://agiliq.com/blog/2012/03/developing-android-applications-from-command-line/[Developing android applications from command line]. Published in agiliq.com, March 20th, 2012.
- [[[Rodriguez]]] Fernando Rodriguez Romero. https://www.bignerdranch.com/blog/learning-android-development-ios-developers-perspective/[Learning Android Development: an iOS Developer's Perspective]. Published in bignerdranch.com, February 20th 2014.
- [[[Savvy]]] Savvy. http://savvyapps.com/blog/how-to-start-android-development-with-an-ios-background[How to Start Android Development with an iOS Background]. Published in savvyapps.com, March 17th 2016.
- [[[Shekhar]]] Amit Shekhar. https://medium.freecodecamp.com/android-development-best-practices-83c94b027fd3#.7pe415xh9[Android Development Best Practices]. Published in Medium.com, September 30th, 2016.
- [[[Sherman]]] Jeremy Sherman. https://www.bignerdranch.com/blog/write-better-code-using-kotlins-require-check-and-assert/[Write Better Code Using Kotlin's Require, Check and Assert]. Published in bignerdranch.com, September 27th, 2017.
- [[[Smith]]] Craig Smith. http://expandedramblings.com/index.php/android-statistics/[108 Amazing Android Statistics (November 2016)]. Published in expandedramblings.com, November 14th, 2016.
- [[[soulseekah]]] soulseekah. https://codeseekah.com/2012/02/16/command-line-android-development-debugging/[Command Line Android Development: Debugging]. Published in codeseekah.com, February 16th, 2012.
- [[[Vogel]]] Lars Vogel. http://www.vogella.com/tutorials/AndroidTestingEspresso/article.html[Android user interface testing with Espresso - Tutorial]. Published in vogella.com, June 18th, 2016.
- [[[Zhuravleva]]] Anastasiia Zhuravleva. https://pspdfkit.com/blog/2018/simple-kotlin-tricks/[Simple Kotlin Tips for Beginners]. Published in pspdfkit.com, January 2018.


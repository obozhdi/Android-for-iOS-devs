[[chapter_networking]]
= Networking

In this chapter we are going to learn how to use some very common networking technologies used by Android applications to communicate with servers and with other devices on the Internet. Not only that, but we are also going to learn how to display the data from the network in a list similar to a `UITableView`.

== TL;DR

For those of you in a hurry, the table below summarizes the most important pieces of information in this chapter.

[[table_networking]]
.Networking in Android
[format="csv", options="header"]
|===
include::{datadir}/Networking/tldr.csv[]
|===

== Consuming REST Web Services

We are going to start this chapter by creating a very simple application that performs a ((network request)) to a free API provided by the http://www.geonames.org/:[GeoNames] geographical database. The data of this database is freely available, and it is licensed under a https://creativecommons.org/licenses/by/3.0/[Creative Commons Attribution 3.0 License.]

In particular, we are going to use a very nice API they offer, called http://www.geonames.org/export/wikipedia-webservice.html#findNearbyWikipedia["findNearbyWikipedia"] which returns items of interest located in a geographical region. This API returns data in both ((JSON)) and ((XML)) formats, which we will use to show how to parse data in these two different formats.

First we are going to use the default ((HTTP libraries)) offered by Android, and later in this chapter we are going to use https://square.github.io/retrofit/[Retrofit], a third party open source library created by the team of https://squareup.com/[Square].

=== Built-in HTTP Libraries

We are going to start our exploration of Networking in Android by performing a very simple GET request to one of the endpoints in the GeoNames APIs.

[NOTE]
.Follow along
====
The code of this section is located in the `Networking/HTTPRequest` folder.
====

Create a new project in Android Studio, using all the default parameters. Add a new Kotlin class to this project and name it `APIConnector`. The code of the `APIConnector` class is available in the following code snippet.

[[src-networking-basichttp]]
.Class performing a GET HTTP request
[source,kotlin,indent=0]
----
include::{codedir}/Networking/HTTPRequest/app/src/main/java/training/akosma/httprequest/APIConnector.kt[tag=apiconnector]
----

The ((`HttpURLConnection`)) class in the Android SDK fulfills a similar role to that of the `NSURLConnection` class in Cocoa. You can specify various parameters, including the HTTP verb to be used (by default, as one might expect, this value is `GET`) and other parameters, such as headers, cookies, authentication, etc.

[TIP]
.Use the built-in cache
====
Remember how caching was one of the most complex problems in computer science? Do not create your own local cache for downloaded images! Just use the one provided by the `HttpURLConnection` class: `conn.setUseCaches(true);` and you are ready to go.
====

We are now going to use this simple wrapper around the `HttpURLConnection` class in our `MainActivity`. We could simply do the following at this stage:

.Blocking the Main Thread
[source,kotlin,indent=0]
----
@Override
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    String data = new APIConnector().getStringData(API_URL);
}
----

But this approach has a major flaw; it runs the network connection on the main thread of the application. And this is a bad, bad thing; just like it is in iOS. No surprise here.

Android applications, like most GUI toolkits out there, use an "event loop" in the main thread. This event loop, represented by the ((`Looper`)) class, and very well known to any iOS developer used to see the `NSRunLoop` class out there in the wild, performs pretty much the same tasks in Android as it does in iOS. It consumes events from the operating system, and executes the code associated to these events as fast as possible. If we use the `APIConnector` class in our main loop, it will block its execution until the network call has completed (or failed for any reason, including a timeout.) We say that the network call is executed _synchronously_.

What we need, in this case, is a mechanism to execute our network call in a background thread, or _asynchronously_, and for this reason we are going to use the standard ((`android.os.AsyncTask`)) class.

A much better version of the `MainActivity` class looks like this:

.Performing Network Operations in the Background
[source,kotlin,indent=0]
----
include::{codedir}/Networking/HTTPRequest/app/src/main/java/training/akosma/httprequest/MainActivity.kt[tag=activity]
----

The result of running the code of this application should appear in the logcat viewer of your Android Studio installation, and it should look like this:

.JSON API Output
[source,indent=0]
----
include::{codedir}/Networking/HTTPRequest/output.txt[lines="1..15"]
----

The benefit of using an `AsyncTask` is that our main thread is now completely free to keep receiving input and events, and our user will be able to scroll, navigate and perform any other task while the network call returns – or not.

TIP: The JSON result contains information about the points of interest in a canton of Switzerland called Glaris – a beautiful place you should definitely visit one day! If you do, do not forget to let the author know and hopefully we could meet in person and talk about Android watching the Swiss Alps.

=== Permissions

If you have followed the instructions above, most probably the code did not work, and that is OK; we have forgotten to add the required ((permissions)) to our application.

By default, and for security reasons, Android applications are not allowed to perform many operations, such as connecting to the internet, accessing the list of contacts on your device or using the camera. You have to manually give permission to perform each of these operations, and this is done in the `AndroidManifest.xml` file.

.Internet Permission in the AndroidManifest.xml file
[source,xml,indent=0]
----
include::{codedir}/Networking/HTTPRequest/app/src/main/AndroidManifest.xml[tag=permission]
----

=== OkHttp

To finish this overview of simple HTTP connectivity, we are going to perform the same operation as previously, but instead of using `HttpURLConnection` we are going to use an open source library called ((OkHttp)).

https://square.github.io/okhttp/[OkHttp] is a wildly popular Android networking library. It is referenced by many other libraries in the open source world around Android, and in many ways it can be considered the equivalent of https://github.com/AFNetworking/AFNetworking[AFNetworking].

[NOTE]
.Follow along
====
The code of this section is located in the `Networking/OkHttp` folder.
====

Using it is extremely simple, and we are going to reuse the GeoNames API we have used previously. First you must add the dependency in Gradle, as shown below.

[[src-okhttp-gradle]]
.Adding OkHttp as a dependency
[source,groovy,indent=0]
----
include::{codedir}/Networking/OkHttp/app/build.gradle[tag=gradle]
----

Next we are going to rewrite the `APIConnector` class we created before, so that it uses OkHttp instead. And the final code could not be simpler, as shown as follows.

[[src-okhttp-final]]
.Using OkHttp
[source,kotlin,indent=0]
----
include::{codedir}/Networking/OkHttp/app/src/main/java/training/akosma/okhttp/APIConnector.kt[tag=apiconnector]
----

OkHttp offers the full spectrum of services expected of a networking library, and it is very simple to use.

== Parsing JSON Data

The result of our call is a long string containing a certain amount of information codified in the venerable http://json.org/[((JSON))] format. This format has become a _de facto_ standard for web APIs, and as such we are going to see how to convert this JSON code into native data structures that we can manipulate with Kotlin. This process is called *parsing and deserializing* the JSON string.

[NOTE]
.Follow along
====
The code of this section is located in the `Networking/JSONParsing` folder.
====

We are going to create a new project now, using the default parameters as usual, in we are going to reuse our `APIConnector` class from our previous project.

This time we are going to create a ((POJO)) (also known as "Plain Old Java Object") that we will call `PointOfInterest` – it will hold the information returned by the JSON returned from the API call.

.A POJO Representing a Point of Interest
[source,kotlin,indent=0]
----
include::{codedir}/Networking/JSONParsing/app/src/main/java/training/akosma/jsonparsing/PointOfInterest.kt[tag=poi]

// ...
----

=== Parsing JSON with Gson

We all agree that the code in the previous section is quite verbose, and if we have to do the same for every POJO in our application we are going to end up with a substantial amount of boilerplate code scattered throughout our application.

We can do better, because Google thankfully provides https://github.com/google/gson[((Gson))], an open source library that removes the need for adding this code manually for every deserialized object in our application. Gson helps developers to serialize and deserialize JSON structures into native Kotlin objects fast, easily and efficiently. It can handle large amounts of data and is very fast.

[IMPORTANT]
.Equivalent of Gson in iOS
====
In the world of iOS apps, https://github.com/Mantle/Mantle[Mantle] plays more or less the same role than Gson. You can also think of Gson as an equivalent of Cocoa's `NSPropertyListSerialization` class, but instead of working with Cocoa's native "property list formats" (Binary and XML), Gson works with JSON strings and streams.
====

First we need to add the dependency to our application-level Gradle file, as shown here:

[NOTE]
.Follow along
====
The code of this section is located in the `Networking/Gson` folder.
====

[[src-gson-gradle]]
.Adding Gson as a dependency in the build.gradle file
[source,groovy,indent=0]
----
include::{codedir}/Networking/Gson/app/build.gradle[tag=gradle]
----

Once this is done, we can modify the `APIConnector` class.

[[src-gson-apiconnector]]
.APIConnector class using Gson
[source,kotlin,indent=0]
----
include::{codedir}/Networking/Gson/app/src/main/java/training/akosma/gson/APIConnector.kt[tag=decode]
----

And that is all. Because our `PointOfInterest` class has exactly the same field names as the JSON returned by the API, we can use Gson here to perform a very simple 1-to-1 mapping of fields and keys.

== Parsing XML Data

The same web service we have used so far can return ((XML)) data; the only difference is that we have to remove the "JSON" word from the URL, and just by using the same parameters, we will have a simple XML output.

[NOTE]
.Follow along
====
The code of this section is located in the `Networking/XMLParsing`
folder.
====

In this section we are going to learn how to use the standard "SAX-style" XML parser functionality built-in into Android.

=== About XML Parsers

There are two different kinds of XML parsers:

1. SAX-style
2. DOM-style

A ((SAX-style XML parser)) is event-based, and consume a stream of data; it is usually very fast, requires very little RAM, and is perfectly adapted for the low-power, battery-fed world of smartphones. On the other hand, it is usually hard to manipulate and code, particularly if the XML stream is complex.

On the other hand, a ((DOM-style XML parser)) loads a whole XML file in memory at once, and offers a tree-based approach with nodes and children nodes. This is usually a much simpler programming model, but it has increased memory requirements, which makes it suitable only for small amounts of XML data.

=== Using The Android SAX XML Parser

The XML data returned from the web service looks like the output below.

[[src-xml-data]]
.XML data returned by the web service
[source,xml,indent=0]
----
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<geonames>
<entry>
<lang>en</lang>
<title>Glärnisch</title>
<summary>The Glärnisch is a mountain… </summary>
<feature>mountain</feature>
<countryCode>CH</countryCode>
<elevation>2880</elevation>
<lat>46.9986</lat>
<lng>8.9986</lng>
<wikipediaUrl>http://en.wikipedia.org/wiki/Gl%C3%A4rnisch</wikipediaUrl>
<thumbnailImg/>
<rank>91</rank>
<distance>0.1869</distance>
</entry>
<!-- ... -->
</geonames>
----

The code required to parse this XML stream is located in the `training.akosma.xmlparsing.XmlPOIParser` class in the sample project. The most important method of that class is reproduced below, and shows how the parser advances through the XML stream until there is no more data to process.

[[src-xml-parser-1]]
.XML SAX parser
[source,kotlin,indent=0]
----
include::{codedir}/Networking/XMLParsing/app/src/main/java/training/akosma/xmlparsing/XmlPOIParser.kt[tag=parse]
----

The code above references the `readPOI()` method, which itself watches the stream for specific tags, and builds the required `PointOfInterest` instance accordingly.

[[src-xml-parser-2]]
.XML SAX parser
[source,kotlin,indent=0]
----
include::{codedir}/Networking/XMLParsing/app/src/main/java/training/akosma/xmlparsing/XmlPOIParser.kt[tag=read]
----

The whole process becomes relatively simple thanks to the structure of the XML data, which has no major complexities. Keeping track of the different tags in the XML stream can be problematic using this method. For example, if the same XML tag appear at different levels in the stream, then the developer is forced to keep track of the current "depth" level of the tree and the current tag name, in order to parse the data correctly. This can quickly become complex.

== Displaying Data in Lists

One of the most common tasks that iOS and Android developers perform every day consists in loading data from some backend network service and display it in a list. This is so common that we are going to dedicate a complete section to it, and we are going to discover just how similar it is to use a ((`RecyclerView`)) than to use a `UITableView` instance.

[NOTE]
.Follow along
====
The code of this section is located in the `Networking/PointOfInterest` folder.
====

This sample code consists in a `RecyclerView` instance contained within a `PointOfInterestListFragment` object, itself contained within the `MainActivity` of our class. However, instead of loading the activity through the XML layout – just like we did in chapter "User Interface", this time we are going to load the fragment programmatically using the `FragmentManager` system.

This is a complementary mechanism to that of XML layouts, and allows for greater flexibility; fragments loaded using the Fragment Manager can be replaced, removed and changed at runtime, which is something that XML-based fragments cannot do.

To use the fragment manager is very simple.

[[src-recycler-mainactivity]]
.Loading fragments using a FragmentManager
[source,kotlin,indent=0]
----
include::{codedir}/Networking/PointOfInterest/app/src/main/java/training/akosma/pointofinterest/MainActivity.kt[tag=fragment]
----

The fragment itself contains an instance of the ((`android.support.v7.widget.RecyclerView`)) class, which as the package name implies is part of the Support Libraries of Android. It has been recently added to the platform, and it has quickly become the de-facto mechanism to display lists in Android. It is very simple to use and its API looks very similar to that of the `UITableView` class in iOS.

The ((`RecyclerView`)) class is not available by default in Android projects; the following images show how to add the required dependencies in the project when selecting menu:File[Project Structure] or using the kbd:[{commandkey} + ↓] (arrow down) keyboard shortcut, and then selecting the "Dependencies" tab.

[[img-recycler-import-1]]
.Project dependencies
image::Networking/Library_dependency_01.png[Project dependencies]

[[img-recycler-import-2]]
.Add the RecyclerView dependency in the project
image::Networking/Library_dependency_02.png[Add the RecyclerView dependency in the project]

We need to give the `RecyclerView` the data to be displayed; for that, we have to create an `Adapter` which is an object that extends the ((`RecyclerView.Adapter`)) class. An adapter is nothing else than the "data source" of the recycler view; its role is to return a "Holder" view (what we could simply call a "cell" in iOS) for each item in the list.

This is the adapter for our `RecyclerView`.

[[src-recycler-adapter]]
.Adapter for the RecyclerView
[source,kotlin,indent=0]
----
include::{codedir}/Networking/PointOfInterest/app/src/main/java/training/akosma/pointofinterest/PointOfInterestListFragment.kt[tag=adapter]
----

The `onCreateViewHolder()` method of the adapter is the local equivalent of the `tableView:cellForRowAtIndexPath:` method in the `UITableViewDataSource` protocol in Cocoa. This method returns a subclass of `RecyclerView.ViewHolder` which is in many ways equivalent to the `UITableViewCell` class.

[IMPORTANT]
.Generic Class
====
The `RecyclerView.Adapter` class is generic, and takes as a parameter the class name of the "cell" to be returned for each item in the list.
====

The local subclass of `RecyclerView.ViewHolder` is shown below.

[[src-recycler-viewholder]]
.ViewHolder subclass
[source,kotlin,indent=0]
----
include::{codedir}/Networking/PointOfInterest/app/src/main/java/training/akosma/pointofinterest/PointOfInterestListFragment.kt[tag=holder]
----

Once the application runs, the list appears on screen as shown in the next figure.

[[img-recycler-result]]
.List implemented with the RecyclerView class
image::Networking/ListView.png[List implemented with the RecyclerView class]

=== Fast-Scrolling RecyclerView

Since version 26 of the Android Support Library, the `RecyclerView` class can support fast scrolling, that is, a bar on the rightmost edge on the screen of the list which allows it to scroll very quickly to the bottom. To add this feature, only the XML resource files need to be changed; just add the tags shown below.

[[src-recycler-fast-layout]]
.Fast-scrolling RecyclerView
[source,xml,indent=0]
----
include::{codedir}/Networking/PointOfInterest/app/src/main/res/layout/fragment_list.xml[tags=fast]
----

You will need to add the required drawables in the `res/drawable` folder of your application, for example a line and a thumb marker.

[[src-recycler-fast-line]]
.Line used to draw the fast scrolling feature
[source,xml,indent=0]
----
include::{codedir}/Networking/PointOfInterest/app/src/main/res/drawable/line.xml[tags=line]
----

[[src-recycler-fast-thumb]]
.Thumb marker for fast scrolling
[source,xml,indent=0]
----
include::{codedir}/Networking/PointOfInterest/app/src/main/res/drawable/thumb.xml[tags=thumb]
----

NOTE: For a nicer example, with a much longer list, check the example in the `UI/Fragments` folder.

== Retrofit

https://square.github.io/retrofit/[((Retrofit))] is a high-level, strongly typed REST API wrapper library for Kotlin and Android. It is built on top of ((OkHttp)) and provides a complete abstraction over the entities being served over the API. It is quite simple to use it but it requires a bit of infrastructure to setup.

[NOTE]
.Follow along
====
The code of this section is located in the `Networking/RESTClient`
folder.
====

The application talks to a server application built with https://nodejs.org/[Node.js] available in the `Networking/restapi` folder. This application can be launched locally using the `node app.js` command, and then you should modify line 11 of the `ServiceGenerator` class in the project to point to the correct URL.

[NOTE]
.Using ngrok as reverse proxy
====
For testing purposes the author of this lines was using https://ngrok.com[((ngrok))] to tunnel the local Node.js server so that it could be reached from any testing device.
====

First you need to add the dependency in the `app/build.gradle` file.

[[src-retrofit-gradle]]
.Adding Retrofit as a dependency
[source,groovy,indent=0]
----
include::{codedir}/Networking/RESTClient/app/build.gradle[tag=gradle]
----

Then you need to create the infrastructure required to map the ((REST)) endpoints to your Android application:

1. The `ServiceGenerator` class wraps the creation of the different client objects used to connect to the backend.
2. The `UsersClient` interface maps local Kotlin methods with remote REST API methods, including their HTTP verbs and other contextual information.
3. A local model class `User` represents the data being manipulated through the REST interface.

[[src-retrofit-servicegenerator]]
.Retrofit ServiceGenerator class
[source,kotlin,indent=0]
----
include::{codedir}/Networking/RESTClient/app/src/main/java/training/akosma/restclient/ServiceGenerator.kt[tag=generator]
----

[[src-retrofit-usersclient]]
.UsersClient interface
[source,kotlin,indent=0]
----
include::{codedir}/Networking/RESTClient/app/src/main/java/training/akosma/restclient/UsersClient.kt[tag=client]
----

[[src-retrofit-user]]
.User class
[source,kotlin,indent=0]
----
include::{codedir}/Networking/RESTClient/app/src/main/java/training/akosma/restclient/User.kt[tag=user]

// ...
----

Once all of these elements are in place, we can start creating, editing and deleting users using a relatively high-level, strongly typed interface.

[[src-retrofit-usage]]
.Using the Retrofit infrastructure in your code
[source,kotlin,indent=0]
----
include::{codedir}/Networking/RESTClient/app/src/main/java/training/akosma/restclient/MainActivity.kt[tag=usage]
----

The code speaks by itself; if your network API follows closely the design guidelines of the REST specification, you could benefit greatly from using such a library. One of the biggest benefits it is that it removes all boilerplate code and it makes your application logic stand out at first sight.

== WebView

Android applications can ((display web content)) very easily, just like iOS applications can use `WKWebView` instances to do the same. The ((`android.webkit.WebView`)) class provides such services, and in this section we will create a simple web browser application.

[NOTE]
.Follow along
====
The code of this section is located in the `Networking/Browser` folder.
====

The first thing we need to do is to create a `RelativeLayout` with three subviews:

1. An `EditText` to enter URLs.
2. A button to launch requests.
3. A web view to display the requested page.

Then, on the main activity, we can wire all of these components together:

[[src-networking-browser-setup]]
.Wiring the components of a web browser application
[source,kotlin,indent=0]
----
include::{codedir}/Networking/Browser/app/src/main/java/training/akosma/browser/MainActivity.kt[tags=setup]
----

To be able to receive events from the web browser, we need to create an instance of `android.webkit.WebViewClient` and override some methods:

[[src-networking-browser-client]]
.WebViewClient object to receive events from the WebView
[source,kotlin,indent=0]
----
include::{codedir}/Networking/Browser/app/src/main/java/training/akosma/browser/MainActivity.kt[tags=client]
----

The `go()` method is very simple:

[[src-networking-browser-go]]
.Method triggering the loading of URLs
[source,kotlin,indent=0]
----
include::{codedir}/Networking/Browser/app/src/main/java/training/akosma/browser/MainActivity.kt[tags=go]
----

Finally, in order to provide keyboard and back button support, we can override the `onKeyDown()` method:

[[src-networking-browser-back]]
.Handling keyboard and back button events
[source,kotlin,indent=0]
----
include::{codedir}/Networking/Browser/app/src/main/java/training/akosma/browser/MainActivity.kt[tags=back]
----

The image below shows the result of using the web browser.

[[img-networking-browser]]
.WebView browser app in action
image::Networking/WebView.png[WebView browser app in action]

== Embedding a Web Server in an Application

Many applications feature embedded web servers, which can be useful to share files with other users in the local network or to provide advanced functionality. To do this, we are going to use a popular embeddable web server called http://nanohttpd.org/[((NanoHTTPD))].

[NOTE]
.Follow along
====
The code of this section is located in the `Networking/HTTPServer` folder.
====

As usual, the first step consists in adding the required dependencies in the module Gradle file:

[[src-networking-embed-gradle]]
.Embedding NanoHTTPD in an application
[source,groovy,indent=0]
----
include::{codedir}/Networking/HTTPServer/app/build.gradle[tags=gradle]
----

Then we need to add the required permissions so that we can connect to our device from the network:

[[src-networking-embed-permissions]]
.AndroidManifest.xml permissions
[source,xml,indent=0]
----
include::{codedir}/Networking/HTTPServer/app/src/main/AndroidManifest.xml[tags=permissions]
----

Then we need to create a subclass of `fi.iki.elonen.NanoHTTPD`, which will be used to return responses to HTTP requests.

[[src-networking-embed-server]]
.An HTTP server for our application
[source,kotlin,indent=0]
----
include::{codedir}/Networking/HTTPServer/app/src/main/java/training/akosma/httpserver/Server.kt[tags=server]
----

And the only remaining thing to do now is to actually use this server in our main activity.

[[src-networking-embed-usage]]
.Using our HTTP server from the main activity
[source,kotlin,indent=0]
----
include::{codedir}/Networking/HTTPServer/app/src/main/java/training/akosma/httpserver/MainActivity.kt[tags=usage]
----

If you run your application now, it will display the URL offered by the HTTP server, for example `http://192.168.1.107:8080/`. Open any browser now and navigate to this page, and you will be able to interact with your Android device in a different way.

[NOTE]
.Port 80 is not available
====
For security reasons, port 80 is reserved to the root user in all Linux systems, and as such it is not available when using NanoHTTPD.
====

== Zeroconf

http://www.zeroconf.org/[((Zeroconf))] or Zero-Configuration Networking is defined by https://en.wikipedia.org/wiki/Zero-configuration_networking[Wikipedia] as follows:

[quote, Wikipedia]
____
…a set of technologies that automatically creates a usable computer network based on the Internet Protocol Suite (TCP/IP) when computers or network peripherals are interconnected. It does not require manual operator intervention or special configuration servers.
____

Among its various uses, Zeroconf is particularly popular among printer manufacturers, allowing users in a local network to automatically discover and setup printers in their devices.

Android applications, just like their iOS counterparts, are able to use Zeroconf natively since Android 4.1 (Jelly Bean, API 16.) In this section we are going to learn how to use Zeroconf in Android using the native libraries, as well as using a common substitute, the http://jmdns.sourceforge.net/[JmDNS library] also available in https://github.com/jmdns/jmdns[Github.]

[NOTE]
.Follow along
====
The code of this section is located in the `Networking/Zeroconf` folder.
====

=== Basic Zeroconf Concepts

Zeroconf is a large standard, but most of what people do with it can be summarized in two items:

- Either ((advertise a service)) so that other users in the network can connect to it;
- Or ((browse for services)) to connect to in the local network.

"Services" can be anything you can imagine: a printer, a chat server, a media streaming device, an IoT (Internet of Things) device in your home, a game controller, anything. Applications interested in a particular type of service must know the "service identifier" in the form of a string with this shape:

    _zeroconfserv._tcp.local.

Knowing this simple string, usually provided by the manufacturer of the service or by a Bonjour browser application, you can create applications that connect to a variety of devices.

Android devices, just like iOS ones, can be used to advertise services in the network, and they can also browse for services. Since our application has to have access to the local network, we need to add the proper permissions:

[[src-networking-zeroconf-permissions]]
.AndroidManifest.xml permissions for Zeroconf
[source,xml,indent=0]
----
include::{codedir}/Networking/Zeroconf/app/src/main/AndroidManifest.xml[tags=permissions]
----

=== Creating a Zeroconf Service

To create a Zeroconf service in Android, use the ((`android.net.nsd.NsdServiceInfo`)) class. Once the service is created, one must advertise it, and this requires using the ((`android.net.nsd.NsdManager`)) class:

[[src-networking-zeroconf-server]]
.Zeroconf server in Android
[source,kotlin,indent=0]
----
include::{codedir}/Networking/Zeroconf/app/src/main/java/training/akosma/zeroconf/ZeroconfServer.kt[tags=server]
----

There is an obvious risk of collision of service names in the same network, since many devices could be running this same code at the same time. In that case, the registration process of the service assigns a unique service name at the end of the registration process:

[[src-networking-zeroconf-server-reg]]
.Registration finished for a Zeroconf server
[source,kotlin,indent=0]
----
include::{codedir}/Networking/Zeroconf/app/src/main/java/training/akosma/zeroconf/ZeroconfServer.kt[tags=registration]
----

In the code above, we notify the application of the name that our service has been assigned by the operating system after the registration.

=== Browsing for Zeroconf Services

Devices interested in consuming Zeroconf services can browse for them using another instance of `android.net.nsd.NsdManager`:

[[src-networking-zeroconf-browse-start]]
.Starting to browse for Zeroconf services
[source,kotlin,indent=0]
----
include::{codedir}/Networking/Zeroconf/app/src/main/java/training/akosma/zeroconf/NativeZeroconfClient.kt[tags=start]
----

When a service is found, our code must check that it is the one we are looking for; if it is, then we will ask our manager to resolve it, that is, to find all the information about the service, usually the IP and the port.

[[src-networking-zeroconf-browse-found]]
.Found Zeroconf services and ask for resolution
[source,kotlin,indent=0]
----
include::{codedir}/Networking/Zeroconf/app/src/main/java/training/akosma/zeroconf/NativeZeroconfClient.kt[tags=found]
----

Finally, once the service is resolved, we notify the application about it.

[[src-networking-zeroconf-browse-resolved]]
.Resolved Zeroconf service ready to be consumed
[source,kotlin,indent=0]
----
include::{codedir}/Networking/Zeroconf/app/src/main/java/training/akosma/zeroconf/NativeZeroconfClient.kt[tags=resolved]
----

=== Browsing for Zeroconf Services using JmDNS

Using ((JmDNS)) is very similar, but it requires to download and add a set of JARs from the internet into the `app/libs` folder of the project, and then to right-click on them so that they are added properly to the module Gradle file:

[[src-networking-zeroconf-jmdns-gradle]]
.Gradle file adding JmDNS to the project
[source,groovy,indent=0]
----
include::{codedir}/Networking/Zeroconf/app/build.gradle[tags=gradle]
----

Once this is done, we must use the `javax.jmdns.JmDNS` class to browse the network for services:

[[src-networking-zeroconf-browse-jmdns-start]]
.Starting to browse for Zeroconf services with JmDNS
[source,kotlin,indent=0]
----
include::{codedir}/Networking/Zeroconf/app/src/main/java/training/akosma/zeroconf/JmDNSZeroconfClient.kt[tags=start]
----

Similarly, when a service is found, we ask JmDNS to resolve it:

[[src-networking-zeroconf-browse-jmdns-found]]
.Found Zeroconf services and ask for resolution to JmDNS
[source,kotlin,indent=0]
----
include::{codedir}/Networking/Zeroconf/app/src/main/java/training/akosma/zeroconf/JmDNSZeroconfClient.kt[tags=found]
----

And of course, after the service is resolved, we notify the application about it.

[[src-networking-zeroconf-browse-jmdns-resolved]]
.JmDNS resolved a Zeroconf service
[source,kotlin,indent=0]
----
include::{codedir}/Networking/Zeroconf/app/src/main/java/training/akosma/zeroconf/JmDNSZeroconfClient.kt[tags=resolved]
----

=== Browsing and Resolving a macOS Service

To show the interoperability between the Apple and the Android implementations of the Zeroconf protocol suite (also referred to as "Bonjour" in the world of Apple) there is a Swift program in the `Networking/Zeroconf` folder containing a simple network service, which your Android devices will be connecting to.

[[src-networking-swift]]
.Zeroconf service written in Swift
[source,swift,indent=0]
----
include::{codedir}/Networking/Zeroconf/zeroconf.swift[tags=server]
----

This server can be launched from the command line using the following commands:

    $ swift zeroconf.swift

If you want, you can even launch it directly:

    $ chmod +x zeroconf.swift
    $ ./zeroconf.swift

[[img-networking-zeroconf-mac]]
.Mac application exporting a Zeroconf service
image::Networking/networking_server.png[Mac application exporting a Zeroconf service]

Once launched on your Mac, launch the Android application in your device, and make sure that your device and the Mac are in the same local network (wired or wireless.) Hit the "Native Client" or "JmDNS Client" buttons on your Android device and after a few seconds you should see something like the screenshot below.

[[img-networking-zeroconf-android]]
.Android device found Zeroconf service
image::Networking/networking_bonjour.png[Title]

Hitting the "Server" button in the Android application will advertise a Zeroconf service on the local network, using the same identifier as the Mac version.

[WARNING]
.Issues resolving services in some devices
====
During the preparation of this book, the Samsung Tablet was able to successfully advertise the service to the network, but was totally unable to browse, discover or resolve services. Be aware that there are substantial differences among devices, and that you might encounter some problems using Bonjour in Android.
====

== Summary

The most important thing to keep in mind when writing networking code in Android is, just as with iOS, to keep long running tasks off the main thread. There are many mechanisms to do this, starting with the `AsyncTask` class, which provides a handy solution for short-lived requests. Most networking libraries automatically take their work to a background thread for you.

Android includes a JSON parsing library, but the open source library Gson by Google is wildly popular too; it provides automatic serialization and deserialization of objects to and from any class (or collection thereof.)

Speaking about open source libraries, OkHttp and Retrofit are also popular choices to connect Android applications to backend services. The former can be thought of the Android equivalent of AFNetworking, while Retrofit is more similar to RestKit, at least in spirit.

Finally, consuming web content from Android applications is as simple as using the `android.webkit.WebView` class, which has a very similar API to that of `WKWebView`.

